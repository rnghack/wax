/*
 
   Copyright 2019 RNGHACK

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/

#define _XOPEN_SOURCE 700 

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>


// BASE     ~~~~~~~~~~~~~~~~~~~~~~~
//
#define true  1
#define false 0

#define bool  char

#define s8  char
#define u8  unsigned char

#define s32  int
#define u32  unsigned int

#define s64  long
#define u64  unsigned long

#define line_type_not_found -1

#define line_type_fn_header  1 

#define line_type_parameter  2


struct BASE_TYPE {
  u32  type;
  u64  offset;
};

// ARRAY    ~~~~~~~~~~~~~~~~~~~~~~~
struct ARRAY_TYPE {
  struct    
  BASE_TYPE  base;     
  u32        width;  
  u32        capacity;
  u32        size;   
  u32        offset_start;
  u32        offset_end;
  u32        mark;
};


struct ARRAY_128 {
  struct    
  ARRAY_TYPE  array;     
  u8          body[128];
};

/*
 
   Copyright 2019 RNGHACK

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/

// STRING   ~~~~~~~~~~~~~~~~~~~~~~~
struct STRING {
  struct    
  BASE_TYPE  base;     

  struct    
  ARRAY_128  array_128;     

};


void string$init(struct STRING *string) {
  string->array_128.array.capacity     = 128;
  string->array_128.array.size         = 0;
  string->array_128.array.offset_start = 0;
  string->array_128.array.offset_end   = 0;
  string->array_128.array.mark         = 0;
}


struct STRING *struct_STRING() {
  struct STRING *string;

  string = malloc(sizeof(struct STRING));

  string$init(string);

  return string; 
}


struct STRING *string$make() {
  struct STRING *str = struct_STRING();
  string$init(str);
  return str;
}


void string$clear(struct STRING *str) {
  str->array_128.array.size = 0; 
}


u32 string$len(struct STRING *string) {
  return string->array_128.array.size; 
}


u32 string$capacity(struct STRING *string) {
  return string->array_128.array.capacity; 
}


void string$shrink_by(struct STRING *str, u32 amount) {
  if(amount > string$len(str) ) {
    return;
  }
  str->array_128.array.size -= amount; 
}


void string$null_term(struct STRING *string) {
  // make sure there buffer has enough room
 
  u32 str_size = string->array_128.array.size;
  if(str_size < 127) {
    string->array_128.body[str_size] = 0;
  }
}


char* string$chars(struct STRING *string) {
   return (char *)string->array_128.body;
}


char string$last_char(struct STRING *string) {
  u32 str_size = string->array_128.array.size;

  if(str_size == 0) {
    return 0;
  }
  return string->array_128.body[str_size-1];
} 


s32 string$last_index_of_char(struct STRING *string, u8 the_char) {
  u32 str_size = string->array_128.array.size;

  if(str_size == 0) {
    return -1;
  }
  u32 i = str_size - 1;
  bool found = false;

  while(i > 0 && found == false) {
    if(string->array_128.body[i] == the_char) {
      return i;
    } 
    i--;
  } 
  return i; 
}


void string$from_c_string(struct STRING *string, char *c_string) {
  string$init(string);

  u32 char_string_len = strlen(c_string); 
  u32 copy_amount = char_string_len;

  if(copy_amount > 128) {
    copy_amount = 128;
  }
  for(s32 i = 0; i <= copy_amount; i++) {
    string->array_128.body[i] = c_string[i];
  }
  string->array_128.array.size = copy_amount;
}


// TODO write test for below
void string$from_c_string_limit(struct STRING *string, char *c_string, u32 limit) {
  string$init(string);

  u32 char_string_len = strlen(c_string); 
  u32 copy_amount = char_string_len;

  if(copy_amount > 128) {
    copy_amount = 128;
  }
  if(limit < copy_amount) {
    copy_amount = limit;
  }
  for(s32 i = 0; i <= copy_amount; i++) {
    string->array_128.body[i] = c_string[i];
  }
  string->array_128.array.size = copy_amount;
}


char *string$to_c_string(struct STRING *string) {
  string$null_term(string);
  return (char *)string->array_128.body;
}


void string$reverse(struct STRING *string) {
  // abc
  // abcde

  u32 len = string$len(string);

  if(len < 2) {
    return;
  }
  len--;

  u32 i       = 0; 
  char *chars = string$chars(string);
  char x;

  while(i < len) {
    x = chars[i];
    chars[i]   = chars[len];
    chars[len] = x; 
    
    i++;
    len--;
  }
}


bool string$append(struct STRING *str1, struct STRING *str2) {
  u32 str1_len      = string$len(str1);
  u32 str1_capacity = string$capacity(str1);

  u32 str2_len = string$len(str2);

 
  // check that the is enough remove in str1
  if(str1_len + str2_len > str1_capacity) {
    return false;
  }

  // copy the bytes
  for(u32 i = 0; i < str2_len; i++) {
    str1->array_128.body[str1_len + i] = str2->array_128.body[i];
  }
  
  // update the size of str1
  str1->array_128.array.size += str2_len;
  return true;
}



bool string$append_char(struct STRING *string, char c) {
  u32 string_len      = string$len(string);
  u32 string_capacity = string$capacity(string);
 
  // check that the is enough room for the extra char
  if(string_len + 1 > string_capacity) {
    return false;
  }

  // copy the char in
  string->array_128.body[string_len] = c;
  
  // update the size of str1
  string->array_128.array.size++;
  return true;
}


bool string$append_chars(struct STRING *string, char *chars) {
  u32 string_len      = string$len(string);
  u32 string_capacity = string$capacity(string);

  u32 chars_len = strlen(chars);

 
  // check that the is enough room in str1
  if(string_len + chars_len > string_capacity) {
    return false;
  }

  // copy the bytes
  for(u32 i = 0; i < chars_len; i++) {
    string->array_128.body[string_len + i] = chars[i];
  }
  
  // update the size of str1
  string->array_128.array.size += chars_len;
  return true;
}


void string$u32_to_string(struct STRING *string, u32 num) { 
  string$init(string);

  u32 answer;
  u32 val; 

  while((answer = num / 10) > 0) {
    val = num % 10;
    string$append_char(string, val + 48); 
    num = answer;
  }
  
  string$append_char(string, num + 48); 
  string$reverse(string);
}


bool string$append_u32(struct STRING *string, u32 num) { 
  struct STRING buffer;

  string$u32_to_string(&buffer, num);
  
  return string$append(string, &buffer); 
}


void string$append_padding(struct STRING *string, 
		           u8 the_char, 
			   u32 padding) { 
  
  u32 len   = string$len(string); 
  s32 count = padding - len;

  if(count < 1) {
    printf("PPPPPPPPPPPPPPPP %d %d\n", padding, len);
    return;
  } 
 
  if(count + len > string$capacity(string)) {
    printf("QQQQQQQQQQQQQQQ\n");
    return;
  }  

  for(u32 i = 0; i < count; i++) {
    string->array_128.body[len + i] = the_char;  
  } 
  string->array_128.array.size += count;
}


bool string$append_chars_section(struct STRING *string, 
                                 u32 start, u32 count,
				 char *chars) {
  if(count == 0) {
    return true;
  }

  u32 string_len      = string$len(string);
  u32 string_capacity = string$capacity(string);

  if(string_len + count - start > string_capacity) {
    return false;
  } 

  for(u32 i = 0; i < count; i++) {
    string->array_128.body[string_len + i] = chars[start + i];
  } 

  // update the size of str1
  string->array_128.array.size += count - start;
  return true;
}


bool string$copy_append(struct STRING *string, u32 start, u32 count) {
  u32 str_len      = string$len(string);
  u32 str_capacity = string$capacity(string);

  // check that the is enough remove in str1
  if(str_len + count > str_capacity) {
    return false;
  }

  // TODO check copy range is valid
  if(start + count > str_len)  {
    return false;
  }
  
  // copy the bytes
  for(u32 i = 0; i < count; i++) {
    string->array_128.body[str_len + i] = string->array_128.body[start + i];
  }
   
  string->array_128.array.size += count;

  return true;
  
}


void string$copy(struct STRING *src, struct STRING *dest) {
  string$init(dest);
  u32 len = src->array_128.array.size;

  for(u32 i = 0; i < len; i++) {
    dest->array_128.body[i] = src->array_128.body[i]; 
  }

  dest->array_128.array.size = len; 
}


bool string$copy_chars(struct STRING *dest, char *chars, u32 start, u32 count) {
  string$init(dest);
  u32 len       = dest->array_128.array.capacity; 
  u32 chars_len = strlen(chars);

  if(start + count > len) {
    return false;
  }

  for(u32 i = 0; i < count; i++) {
    dest->array_128.body[i] = chars[start + i]; 
  }

  dest->array_128.array.size = count; 

  return true;
}


bool string$copy_string(struct STRING *dest, 
		        struct STRING *src, 
			u32 start, u32 count) {
  string$init(dest);
  u32 len       = dest->array_128.array.capacity; 
  u32 chars_len = string$len(src);

  if(start + count > len) {
    return false;
  }

  for(u32 i = 0; i < count; i++) {
    dest->array_128.body[i] = src->array_128.body[start + i]; 
  }

  dest->array_128.array.size = count; 

  return true;
}


bool string$insert_chars(struct STRING *string, char *chars) {
  u32 string_len      = string$len(string);
  u32 string_capacity = string$capacity(string);

  u32 chars_len = strlen(chars); 
 
  // check that the is enough remove in str1
  if(string_len + chars_len > string_capacity) {
    return false;
  }

  // move the existing chars up
  for(u32 i = string_len; i > 0; i--) {
    string->array_128.body[chars_len + i - 1] = string->array_128.body[i - 1];
  }
  
  // copy in the new chars
  for(u32 i = 0; i < chars_len; i++) {
    string->array_128.body[i] = chars[i];
  }

  // update the size of str1
  string->array_128.array.size += chars_len;
  return true;
}



bool string$replace(struct STRING *string, u32 start, u32 amount, struct STRING *new) {
  u32 old_len = string$len(string);
  u32 string_capacity = string$capacity(string);

/* TODO
  printf("replace \n");
  printf("  start: %d, amount: %d old_len: %d, string_capacitiy: %d\n",
	 start, amount, old_len, string_capacity);
*/
  // validate input
  if(start + amount >=  old_len) {
    return false;
  }

  // check the capacity
  u32 new_len  = string$len(new); 
  u32 new_size = old_len - amount + new_len;

  if(new_size > string_capacity) {
    return false;
  } 

  s32 change       = new_len - amount;
  u32 copy_to      = start + new_len;
  u32 save_amount  = old_len - (start + amount);
/* TODO 
  printf("amount: %d\n", amount);
  printf("change:  %d\n", change);
  printf("copy_to:  %d\n", copy_to);
  printf("save_amount:  %d\n", save_amount);
*/
  if(change > 0) {  // moving to the right 
  // TODO  printf("moving right\n");
    for(u32 i = save_amount; i > 0; i--) {
      string->array_128.body[copy_to + i - 1] = string->array_128.body[start + amount + i - 1];
    } 
  } else {         // moving to the left
    // TODO printf("moving left\n");

    for(u32 i = 0; i < save_amount; i++) {
      string->array_128.body[copy_to + i] = string->array_128.body[start + amount + i];
    } 
  } 

  // copy the data in
  for(u32 i = 0; i < new_len; i++) {
    string->array_128.body[start + i] = new->array_128.body[i];
  } 
  // set the new size
  string->array_128.array.size = new_size;
/*
  printf("-----------\n");
  printf("%s\n", string$to_c_string(string));

  printf("-----------\n");
  */

  return true;
}


u32 string$size(struct STRING *string) {
  return string->array_128.array.size;
}


u8 string$char_at(struct STRING *string, s32 index) {
  u32 str_size = string$size(string);
  
  if(index < 0) {
    return 0;
  }

  if(index >= str_size) {
    return 0;
  } 

  return string->array_128.body[index];
}


bool string$char_before_equals(struct STRING *string, s32 index, char the_char) {
  // check that the index > 0 and < string$size
  u32 str_size = string$size(string);
  index--;


  if(index >= str_size) {
    return false;
  } 
  if(index < 0) {
    return false;
  }

  if(string$char_at(string, index) == the_char) {
    return true;
  }  
  return false;
}


bool string$char_after_equals(struct STRING *string, s32 index, char the_char) {
  // check that the index is less that the last character 
  u32 str_size = string$size(string);
  index++;

  if(index >= str_size) {
    return false;
  } 
  if(index == 0) {
    return false;
  }
  if(string$char_at(string, index) == the_char) {
    return true;
  }  
  return false;  
}


bool string$equals(struct STRING *string1, struct STRING *string2) {
  // check length
  u32 s1_len = string1->array_128.array.size;

  if(s1_len != string2->array_128.array.size) {
    return false;
  }

  // offset TODO: FIX probably need to check how STRING struct is initialised
  //
  /*
  s32 string1_offset = string1->array_128.array.offset_start;
  s32 string2_offset = string2->array_128.array.offset_start;
  */
  s32 string1_offset = 0;
  s32 string2_offset = 0;

  // compare chars
  for(u32 i = 0; i < s1_len; i++) {
    if(string1->array_128.body[i + string1_offset] 
         != string2->array_128.body[i + string2_offset]) {
      return false;
    }
  }

  return true;
}


bool string$equals_chars(struct STRING *string1, char *chars) {
  // TODO write test for this
  // check length
  u32 s1_len = string1->array_128.array.size;
  u32 s2_len = strlen(chars);

  if(s1_len != s2_len) {
    return false;
  }

  // offset TODO: FIX probably need to check how STRING struct is initialised
  //
  /*
  s32 string1_offset = string1->array_128.array.offset_start;
  s32 string2_offset = string2->array_128.array.offset_start;
  */
  s32 string1_offset = 0;
  s32 string2_offset = 0;

  // compare chars
  for(u32 i = 0; i < s1_len; i++) {
    if(string1->array_128.body[i + string1_offset] 
         != chars[i + string2_offset]) {
      return false;
    }
  }

  return true;
}


bool string$char_at_in(struct STRING *string, 
		       struct STRING *chars,
		       s32 index) {

  // get the char and check for index validity
  u8  the_char  = string$char_at(string, index);
  s32 chars_len = string$size(chars);

  if(the_char == 0) {
    return false;
  }

  // loop through each char
  for(s32 i = 0; i < chars_len; i++) {
    if(the_char == string$char_at(chars, i)) {
      return true;
    }  
  } 
  return false;
}

s32 string$offset_start(struct STRING *string) {
  return string->array_128.array.offset_start;
}


s32 string$offset_end(struct STRING *string) {
  return string->array_128.array.offset_end;
}


void string$trim_with_offset(struct STRING *string) {
  struct STRING wss;  // white space char, TODO: make a constant
  char *wsc = " \t";

  string$from_c_string(&wss, wsc); 

  u32  str_len = string$size(string);

  if(str_len == 0) {
    return;
  }

  // search from beginning 
  bool cont = true;
  s32 i = 0;

  while(cont) {
    cont = string$char_at_in(string, &wss, i);

    i++;
    cont = i < str_len && cont; 
  }
  
  string->array_128.array.offset_start = i - 1;

  // search from end 
  i = str_len -1;
  s32 j = 0;
  cont = true;
  while(cont > 0) {
    cont = string$char_at_in(string, &wss, i);

    i--;
    j++;
    cont = i > 0 && cont; 
  }
  string->array_128.array.offset_end = j - 1;
}


s32 string$mark(struct STRING *string) {
 return string->array_128.array.mark;
}


s32 string$index_of(struct STRING *string, u8 c) {
  u32 size = string->array_128.array.size;
  
  if(size < 1) {
    return -1;
  } 

  char *chars = (char *)string->array_128.body;
  s32 i = 0;

  while(size > 0) {
    if(chars[i] == c) {
      return i;
    }

    i++;
    size--;
  }

  return -1;
} 


bool string$start_with_char(struct STRING *str, char the_char) {
  u32 chars_len       = str->array_128.array.size;;

  if(chars_len == 0) {
    return false;
  }

  if(str->array_128.body[0] == the_char) {
    return true;
  }

  return false; 
}


bool string$start_with_chars(struct STRING *str, char *start_chars) {
  u32 chars_len       = str->array_128.array.size;;
  u32 start_chars_len = strlen(start_chars);

  if(start_chars_len > chars_len || chars_len == 0 || start_chars_len == 0) {
    return false;
  }

  u32 i = start_chars_len - 1;

  while(i < start_chars_len) {
    if(str->array_128.body[i] != start_chars[i]) {
      return false;
    }
    i++;
  } 

  return true; 
}


bool string$chars_start_with(char *chars, char *start_chars) {
  u32 chars_len       = strlen(chars);
  u32 start_chars_len = strlen(start_chars);

  if(start_chars_len > chars_len || chars_len == 0 || start_chars_len == 0) {
    return false;
  }

  u32 i = start_chars_len - 1;

  do {
    if(chars[i] != start_chars[i]) {
      return false;
    }
    i--;
  } while(i > 0);

  return true; 
}


s32 string$chars_start_with_ignore_white_space(char *chars, char *start_chars) {
  u32 chars_len       = strlen(chars);
  u32 start_chars_len = strlen(start_chars);
  u32 start_at        = 0 ;

  if(chars_len < 1 || start_chars == 0) {
    return -1;
  } 
  // skip any white space
  u8 c;
  bool cont = true;

  while(cont && start_at < chars_len) {
    c = chars[start_at];

    if(c != ' ' && c != '\t') {
      cont = false;
    }
    start_at ++; 
  } 

  if(start_at > 0) {
    start_at --;
  }

  // check to see if there is still enough room for start_chars 
  if(start_at + start_chars_len > chars_len) {
    return -1;
  }
  u32 i = 0;

  // check the start
  while(i < start_chars_len) {
    if(chars[i + start_at] != start_chars[i]) {
      return -1;
    }
    i++;
  } 
  
  return start_at; 
}



// TODO write test
s32 string$chars_index_of(char *chars, u8 c) {
  u32 size = strlen(chars);
  
  if(size < 1) {
    return -1;
  } 

  s32 i = 0;

  while(size > 0) {
    if(chars[i] == c) {
      return i;
    }

    i++;
    size--;
  }

  return -1;
}



// TODO write test
s32 string$chars_index_of_from(char *chars, u8 c, u32 start_at) {
  u32 size = strlen(chars);
  
  if(size < 1 || start_at >= size) {
    return -1;
  } 

  s32 i = start_at;
  size -= start_at;

  while(size > 0) {
    if(chars[i] == c) {
      return i;
    }

    i++;
    size--;
  }

  return -1;
}


u32 string$chars_to_u32(char *chars, u32 offset) {
  u32 num = 0;
  u32 i = 0;

  char c = chars[i + offset];
 
  while(c >= 48 && c <= 57 && i < 10) {
    if(i > 0) {
      num *= 10; 
    }

    num += c - 48;
     
    i++;
    c = chars[i + offset];
  }
  
  return num;
}


void print(struct STRING *str) {
  string$null_term(str);

  char *chars;
  chars = string$chars(str);

  printf("%s", chars);
}


void println(struct STRING *str) {
  string$null_term(str);

  char *chars;
  chars = string$chars(str);

  printf("%s\n", chars);
}


s32 string$index_of_in_using_offset(struct STRING *string, 
				    struct STRING *chars,
				              s32 index) {

  u32 size = string->array_128.array.offset_end;
  s32    i = string->array_128.array.offset_start;

  printf("offset start: %d", i );
  printf("offset end:   %d", size );
  
  if(size < 1) {
    return -1;
  } 

  printf("size: %d\n", size);

  printf("offset_start: %d\n", size);

  while(i < size) {
    bool is_found = string$char_at_in(string, chars, i);

    if(is_found == true) {
      // TODO: ajust for offset 
      return i;
    }

    i++;
  }

  return -1;
}


// TODO bool for split on strings
// TODO refactor process line to use this
u32 string$split_to_array(char *line_chars, struct STRING *line[]) {
  u32 line_len = strlen(line_chars);
  if(line_len == 0) {
    return 0;
  } 

  u32    index              = 0;
  u32    i                  = 0;
  bool   started            = false;
  bool   string_mode        = false;
  struct STRING *working_str = string$make(); 

  // Split line into strings 
  while(i < line_len) {
    if(line_chars[i] == '\n') {
      // ignore
    } else if(string_mode == false 
	       && (line_chars[i] == ' ' || line_chars[i] == '\t')) {

      if(started != false) {
        // add the string to the array list
	line[index] = working_str;
	index++;
	working_str = string$make();
	started = false;
      }
    } else if(line_chars[i] == '\"') {
      if(string_mode == false) {
        string_mode = true; 
      } else if(string_mode == true) {
        string_mode = false; 
        string$append_char(working_str, line_chars[i]);
      }
    } else {
      // Copy char
      started = true;
      string$append_char(working_str, line_chars[i]);
    }
    i++;
  }
  if(started) {
    line[index] = working_str;
    index++;
  }
  return index; // index is the word count;
}


// TESTS --------------------------------------


u32 string_test$index_of_in_using_offset() { 

  char *cstring = "  sub a, b";
  struct STRING str;
  string$from_c_string(&str, cstring);

  char *cchars = " ,\t";
  struct STRING chars;
  string$from_c_string(&chars, cchars);

  string$trim_with_offset(&str);

  s32 result = string$index_of_in_using_offset(&str, &chars, 0);

  if(result != 3) {
    printf("***FAILED*** result: %d\n", result);
    return 1;
  } 
	/*
  result = string$index_of(&str, 'Q');

  if(result != -1) {
    printf("***FAILED*** should not exist.\n");
    return;
  }
  */

  return 0; 
}


u32 string_test$append() {
  

  char *str1 = "hello";
  char *str2 =   " world";
  char *result = "hello world";

  struct STRING str_1;
  string$from_c_string(&str_1, str1);
  struct STRING str_2;
  string$from_c_string(&str_2, str2);
  struct STRING str_result;
  string$from_c_string(&str_result, result);

  bool success = string$append(&str_1, &str_2);

  if(success == false) {
    printf("***FAILED*** (append returned false)\n");

    printf("str1_len: %d", string$len(&str_1));
    printf("str1_capacity: %d", string$capacity(&str_1));
    printf("str2_len: %d", string$len(&str_2));
    
    return 1;
  }

  if(!string$equals(&str_1, &str_result)) {
    printf("***FAILED*** (appended string not equal to expected result)\n");
    println(&str_1);

    printf("str1_len: %d", string$len(&str_1));
    printf("str1_capacity: %d", string$capacity(&str_1));
    printf("str2_len: %d", string$len(&str_2)); 

    return 1;
  }
  return 0;
}


u32 string_test$copy() {

  char *src_c = "copy this string";

  struct STRING src;
  string$from_c_string(&src, src_c);

  struct STRING dest;
  string$copy(&src, &dest);

  char *expected_c = "copy this string";
  struct STRING expected; 

  string$from_c_string(&expected, expected_c);
  
  if(!string$equals(&expected, &dest)) {
    printf("***FAILED*** (copied string not equal to expected result, did not expect: %s\n", 
	   string$to_c_string(&dest));

    return 1;
  }

  return 0;
}


u32 string_test$copy_chars() {


  struct STRING dest;

  char *chars = "copy this string";
  string$copy_chars(&dest, chars, 5, 11);

  char *expected_c = "this string";
  struct STRING expected; 

  string$from_c_string(&expected, expected_c);
  
  if(!string$equals(&expected, &dest)) {
    printf("***FAILED*** (copied string not equal to expected result, did not expect: %s\n", 
	   string$to_c_string(&dest));

    return 1;
    
  }

  return 0;

}


u32 string_test$from_c_string() {

  char *cstring = "hello";
  struct STRING str;

  string$from_c_string(&str, cstring);
  string$null_term(&str);

  if(str.array_128.array.size != strlen(cstring)) {
    printf("***FAILED*** (strings not the same length)\n");
    return 1;
  }

  if(cstring[0] != 'h') {
    printf("***FAILED*** (char[0] doesn't match)\n");
    return 1;
  }
  if(cstring[1] != 'e') {
    printf("***FAILED*** (char[1] doesn't match)\n");
    return 1;
  }
  if(cstring[2] != 'l') {
    printf("***FAILED*** (char[2] doesn't match)\n");
    return 1;
  }
  if(cstring[3] != 'l') {
    printf("***FAILED*** (char[3] doesn't match)\n");
    return 1;
  }
  if(cstring[4] != 'o') {
    printf("***FAILED*** (char[4] doesn't match)\n");
    return 1;
  }
  return 0;
}


u32 string_test$char_before_equals() {
  char *cstring = "add : sum s32";
  struct STRING str;

  bool found = string$char_before_equals(&str, 5, ':');

  if(found == true) {
    return 0;
  } else {
    printf("Failed.\n");   
    return 1;
  } 
}

u32 string_test$char_after_equals() {
  char *cstring = "add : sum s32";
  struct STRING str;
  string$from_c_string(&str, cstring);

  bool found = string$char_after_equals(&str, 4, ' ');

  if(found == true) {
    return 1;
  } else {
    printf("Failed.\n");   
    return 1;
  }
  return 0;
}


u32 string_test$index_of() {

  char *cstring = "add : sum s32";
  struct STRING str;
  string$from_c_string(&str, cstring);

  s32 result = string$index_of(&str, ':');

  if(result != 4) {
    printf("***FAILED*** cannot find : char\n");
    return 1;
  }

  result = string$index_of(&str, 'Q');

  if(result != -1) {
    printf("***FAILED*** should not exist.\n");
    return 1;
  }

  return 0;
}


u32 string_test$chars_index_of_from() {

  char *cstring = "src/wax/wax.c:1402:49:";

  s32 result = string$chars_index_of_from(cstring, ':', 14);

  if(result != 18) {
    printf("***FAILED*** cannot find : char\n");
    return 1;
  }

  return 0;
}


u32 string_test$char_at_in() {

  char *cstring = "add : sum s32";
  char *cchars =   "bcd";
  struct STRING str;
  string$from_c_string(&str, cstring);
  struct STRING chars;
  string$from_c_string(&chars, cchars);

  bool result = string$char_at_in(&str, &chars, 1);

  if(result != true) {
    printf("***FAILED*** cannot find the chars\n");
    return 1;
  }

  result = string$char_at_in(&str, &chars, 3);

  if(result != false) {
    printf("***FAILED*** should not exist.\n");
    return 1;
  }

  return 0;
}


u32 string_test$chars_start_with() {

  char *cstring = "src/wax/wax.c:1402:49:";
  char *cstarts = "src/wax/wax.c";

  if(!string$chars_start_with(cstring, cstarts)) {
    printf("***FAILED*** expect to find %s at the beginning of %s\n", cstarts, cstring);
    return 1;
  }

  return 0;
}


u32 string_test$chars_start_with_ignore_white_space() {

  char *cstring = "   src/wax/wax.c:1402:49:";
  char *cstarts = "src/wax/wax.c";

  s32 result = string$chars_start_with_ignore_white_space(cstring, cstarts);

  if(result < 0) {
    printf("***FAILED*** expect to find %s at the beginning of %s\n", cstarts, cstring);
    return 1;
  }

  return 0;
}


u32 string_test$equals() {

  char *str1 = "abcdef";
  char *str2 = "abcdef";
  char *str3 = "abcdeg";

  struct STRING str_1;
  string$from_c_string(&str_1, str1);
  struct STRING str_2;
  string$from_c_string(&str_2, str2);
  struct STRING str_3;
  string$from_c_string(&str_3, str3);

  bool result = string$equals(&str_1, &str_2);

  if(result != true) {
    printf("***FAILED*** Strings weren't equal\n");
    println(&str_1);
    println(&str_2);
    return 1;
  }

  result = string$equals(&str_2, &str_3);

  if(result != false) {
    printf("***FAILED*** Strings should not be equal.\n");
    println(&str_2);
    println(&str_3);
    return 1;
  }

  return 0;
}


u32 string_test$trim_with_offset() {

  char *cstring = "\t .\t ";
  struct STRING str;
  string$from_c_string(&str, cstring);

  string$trim_with_offset(&str);

  s32 os = string$offset_start(&str);
  s32 oe = string$offset_end(&str);

  if(os != 2) {
    printf("***FAILED*** offset start incorrect\n");
    return 1;
  }
  if(oe != 2) {
    printf("***FAILED*** offset end incorrect\n");
    return 1;
  }

  cstring = "abc";
  string$from_c_string(&str, cstring);

  string$trim_with_offset(&str);

  os = string$offset_start(&str);
  oe = string$offset_end(&str);

  if(os != 0) {
    printf("***FAILED*** nothing to trim offset start incorrect\n");
    printf("offset start %d\n", os);
    return 1;
  }
  if(oe != 0) {
    printf("***FAILED*** nothing to trim offset end incorrect\n");
    printf("offset end %d\n", oe);
    return 1;
  }

  return 0;
}


u32 string_test$insert_chars() {

  char *cstring = "world";
  struct STRING str;
  string$from_c_string(&str, cstring);

  char *new_chars = "hello ";

  char *c_result = "hello world";
  struct STRING result;
  
  string$from_c_string(&result, c_result);
  string$insert_chars(&str, new_chars);

  if(string$equals(&str, &result) != true) {
    printf("***FAILED*** result no expected, got: ");   
    printf("[");   
    print(&str);
    printf("]");   
    printf(" instead of [hello world]");
    return 1; 
  } 

  return 0;
}


u32 string_test$append_chars() {

  char *cstring = "hello";
  struct STRING str;
  string$from_c_string(&str, cstring);

  char *new_chars = " world";

  char *c_result = "hello world";
  struct STRING result;
  
  string$from_c_string(&result, c_result);
  string$append_chars(&str, new_chars);

  if(!string$equals(&str, &result)) {
    printf("***FAILED*** result no expected, got: ");   
    printf("[");   
    print(&str);
    printf("]");   
    printf(" instead of [hello world]");
    return 1; 
  } 

  return 0;  
}


u32 string_test$copy_append() {

  char *c_test = "hello world";
  struct STRING test;
  string$from_c_string(&test, c_test);

  char *c_result = "hello worldworld";
  struct STRING result;
  string$from_c_string(&result, c_result);

  string$copy_append(&test, 6, 5);

  if(!string$equals(&test, &result)) {
    printf("***FAILED*** result no expected, got: ");   
    printf("[");   
    print(&result);
    printf("]");   
    printf(" instead of [hello worldworld]\n");
    return 1; 
  } 

  return 0;  
}


u32 string_test$chars_to_u32() {

  char *chars = "hello 142 world";
  u32  result = string$chars_to_u32(chars, 6);

  if(result != 142) {
    printf("*** FAILED *** Expected 142 but got: %d\n", result);
    return 1;
  }

  return 0;
}
  

u32 string_test$reverse() {

  char *c_input  = "dlrow";
  char *c_expected = "world";

  struct STRING input;
  string$from_c_string(&input, c_input);

  struct STRING expected;
  string$from_c_string(&expected, c_expected); 

  string$reverse(&input);

  if(!string$equals(&input, &expected)) {
    printf("*** FAILED *** Expected: world but got: %s\n", string$to_c_string(&input));
    return 1;
  } 

  return 0;
}


u32 string_test$u32_to_string() {

  u32 num = 4231;
  char *c_result = "4231";

  struct STRING result;
  string$from_c_string(&result, c_result);

  struct STRING str_num;
  string$u32_to_string(&str_num, num);

  if(!string$equals(&result, &str_num)) {
    printf("*** FAILED *** Expected: 4231 but got: %s\n", string$to_c_string(&str_num));
    return 1;
  }

  return 0;  
}


u32 string_test$replace() {
  // TODO add more tests for replacing empty strings


  // test string with replace string being longer than replace amount
  char *chars1_c = "abc1234def";
  struct STRING str1;
  string$from_c_string(&str1, chars1_c);

  char *new1_c = "567890";
  struct STRING new1;
  string$from_c_string(&new1, new1_c);

  char *result1_c = "abc567890def";
  struct STRING result1;
  string$from_c_string(&result1, result1_c);
  

   string$replace(&str1, 3, 4, &new1);

  if(!string$equals(&str1, &result1)) {

    printf("\n***FAILED*** Expected: %s but got: %s\n", string$to_c_string(&result1), 
		                                        string$to_c_string(&str1));
    return 1;
  }

  // test string with replace string being shorter than replace amount
 
  char *chars2_c = "abc1234def";
  struct STRING str2;
  string$from_c_string(&str2, chars2_c);

  char *new2_c = "PQ";
  struct STRING new2;
  string$from_c_string(&new2, new2_c);

  char *result2_c = "abcPQdef";
  struct STRING result2;
  string$from_c_string(&result2, result2_c);

  string$replace(&str2, 3, 4, &new2);

  if(!string$equals(&str2, &result2)) {

    printf("\n***FAILED*** Expected: %s but got: %s\n", string$to_c_string(&result2), 
		                                        string$to_c_string(&str2));
    return 1;
  }

  // TODO test string with replace string being the same as the replace amount

  // TODO test string with replace string empty (amount is 0) 
  
  // TODO test string with replace string with an empty string 
 
  return 0;  
}


void string$free(struct STRING *string) {
}
/*
 
   Copyright 2019 RNGHACK

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/

struct LIST {
         s32        len;
  struct LIST_ITEM  *first;
  struct LIST_ITEM  *last;
};


struct LIST_ITEM {
  struct LIST       *list;
  struct LIST_ITEM  *next;
         void       *item;
};

// @mem_new
struct LIST *list$make() {
  struct LIST *list;

  list = malloc(sizeof(struct LIST));

  // @todo verify malloc successfull 
  list->len = 0;
  list->first = NULL;
  list->last = NULL;

  return list;
}

// @mem_new
bool list$append(struct LIST *list, void *item) {
  struct LIST_ITEM *list_item;

  list_item = malloc(sizeof(struct LIST_ITEM));

  // @TODO: check that malloc worked... else panic

  // setup the list item
  list_item->list = list;
  list_item->next = NULL;
  list_item->item = item;

  if(list->len == 0) {
    list->first = list_item;
    list->last  = list_item;
  } else {
    list->last->next = list_item;
    list->last = list_item;
  }
  
  list->len ++;

  return true; 
}


void *list$first(struct LIST *list) {
  return list->first;
}

void *list$rest(struct LIST *list) {
  return list->first->next;
}


bool list$empty(struct LIST *list) {
  if(list->len <= 0) {
    return true;
  }
  return false;
}


u32 list$size(struct LIST *list) {
  
  return list->len;
}


struct LIST_ITEM *list$next(struct LIST_ITEM *list_item) {
  return list_item->next;
}


bool list$has_next(struct LIST_ITEM *list_item) {
  if(list_item == NULL) {
    return false;
  }
  return true;
}


void *list$get(struct LIST_ITEM *list_item) {
  return list_item->item;
}


void list$free(struct LIST *list) {
 // TODO  
}

// TESTS =============================================================


u32 list_test$make() {

  struct LIST *list = list$make();

  if(list <= 0) {
    printf("***FAILED*** could not create memory for list\n");
    printf("address: %p\n", list);
    return 1;
  }

  s32 list_size = list->len;

  if(list_size != 0) {
    printf("***FAILED*** list not initialised correctly, size != 0\n");
    printf("list_size: %d\n", list_size);
  }
  // @todo check of struct items
  free(list); 
  return 0;  
}


u32 list_test$append() {
  char* c_str = "hello";
  
  struct STRING str; 
  string$from_c_string(&str, c_str);

  struct LIST *list = list$make();

  list$append(list, &str);

  // @TODO check list length and list_item and item

  free(list);
  return 0;  
}



u32 list_test$iterate() {
  char* c_str = "hello";
  char* c_str1 = "world";
  
  struct STRING str; 
  string$from_c_string(&str, c_str);

  struct STRING str1; 
  string$from_c_string(&str1, c_str1);


  struct LIST *list = list$make();
  struct STRING *p_str;

  list$append(list, &str);
  list$append(list, &str1);
  s32 loop_count = 0;

  if(!list$empty(list)) {
    struct LIST_ITEM *li = list$first(list);
    do {
      p_str = list$get(li);
      string$null_term(p_str);

      // printf("i: %s\n", string$chars(p_str));

      li = list$next(li);
      loop_count ++;

    } while(list$has_next(li));
  }

  if(loop_count != 2) {
    printf("***FAILED*** did not loop the expected times: 2\n");
    printf("***FAILED*** actually looped: %d\n", loop_count);
    return 1;
  }
  return 0;  
}


FILE *file$open_r(char *path) {
  FILE *fp = fopen(path, "r");
  return fp;
}


FILE *file$open_w(char *path) {
  FILE *fp = fopen(path, "w");
  return fp;
} 


u32 file$close(FILE *file_handle) {
  fclose(file_handle);
  return 0;
}

// TODO complete
u32 file$read(u32 file_handle, char* buffer, s32 read_size) {
  // read = fread(buffer, read_size, 1, file_handle);
  return 0;
}


// TODO complete
u32 file$write(u32 file_handle, struct ARRAY_128 *buffer) {
  return 0;
}


u32 file$cat(char *path) { 
  FILE *fp = file$open_r(path);

  if(fp == 0) {
    return 0; // TODO should we return -1 here for an error???
  }

  char *line   = NULL;
  size_t len   = 10;
  ssize_t read; 

  while ((read = getline(&line, &len, fp)) != -1) {
    printf("%s", line);
  } 

  return file$close(fp); 
}


bool file$exists(char *path) {
  if(access(path, F_OK) != -1) {
    return true;
  } 
  return false; 
} 
/*
 
   Copyright 2019 RNGHACK

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/ 

/*

   TODO

   BUG: extra spaces at the end of config line
   BUG: extra lines  at the end of config file
*/


// WAX:main ----------------

char* C_LABEL_NAME     = "name:";
char* C_LABEL_TEST     = "test:";
char* C_LABEL_PROGRAM  = "program:";
char* C_LABEL_LOCAL    = "local:";
char* C_LABEL_EXTERNAL = "external:";


struct  STRING LABEL_NAME;
struct  STRING LABEL_TEST;
struct  STRING LABEL_PROGRAM;
struct  STRING LABEL_LOCAL;
struct  STRING LABEL_EXTERNAL;

enum mode_type         { no_mode, name, program_files, external };
enum config_mode_type  { config_no_mode, config_local };

struct CTX {
  bool            has_name;
  bool            has_program;

  enum mode_type         mode;
  enum config_mode_type  config_mode;

  struct STRING   *name;
  
  struct LIST     *test_files; 
  struct LIST     *program_files; 
  struct LIST     *build_info; 

  struct LIST     *config_local;
  struct LIST     *external;

  struct LIST     *tests; 

  u32             test_padding;
  u32             total_line_count;

  FILE            *super_file; 
  FILE            *info_file; 

};


enum external_type { local, git, git_perm };

struct EXTERNAL {
  enum   external_type  ext_type;
  struct STRING         *reference;
  struct STRING         *version;
};

struct EXTERNAL *struct_EXTERNAL() {
  struct EXTERNAL *external;

  external = malloc(sizeof(struct EXTERNAL)); 

  return external; 
}


void print_CTX(struct CTX *ctx) {
  struct STRING    *p_str;
  struct EXTERNAL  *ext;
  struct LIST      *list;
  u32 count;

  printf("CTX {\n  name : ");
  print(ctx->name); 

  // config local
  list = ctx->config_local;
  count = 0;
  printf("\n  config local: ["); 

  if(!list$empty(list)) {
    struct LIST_ITEM *li = list$first(list);
    do {
       p_str = list$get(li);
       print(p_str);
       printf(" ");

      li = list$next(li);
      count++;
    } while(list$has_next(li));
  }
  printf("]");


  // external files
  list = ctx->external;
  count = 0;
  printf("\n  external: ["); 

  if(!list$empty(list)) {
    struct LIST_ITEM *li = list$first(list);
    do {
       ext = list$get(li);
       print(ext->reference);
       printf(" ");

      li = list$next(li);
      count++;
    } while(list$has_next(li));
  }
  printf("]");


  // program files
  list = ctx->program_files;
  count = 0;
  printf("\n  program_files: ["); 

  if(!list$empty(list)) {
    struct LIST_ITEM *li = list$first(list);
    do {
       p_str = list$get(li);
       print(p_str);
       printf(" ");

      li = list$next(li);
      count++;
    } while(list$has_next(li));
  }
  printf("]");



  printf("}\n");  
}


struct CTX *struct_CTX() {
  struct CTX *ctx;

  ctx = malloc(sizeof(struct CTX));

  ctx->has_name = false;
  ctx->has_program = false;

  ctx->mode = no_mode;

  ctx->program_files = list$make();
  ctx->tests = list$make();
  ctx->config_local = list$make();
  ctx->external = list$make();

  ctx->test_padding = 0; 
  ctx->total_line_count = 0;

  return ctx;
}


struct BUILD_INFO {
  struct STRING *module;
  u32           line_count; 
}; 


struct BUILD_INFO *struct_BUILD_INFO() {
  struct BUILD_INFO *build_info;

  build_info = malloc(sizeof(struct BUILD_INFO));

  build_info->line_count = 0;

  return build_info;
};


// TODO convert everything to use words array
bool config_handle_line(struct CTX    *ctx, 
                        char          *line, 
			struct STRING *words[],
			u32           word_count) {

  bool failed = false;
  if(word_count == 0) {
    return failed;
  } 
  // skip line if comment
  if(string$start_with_char(words[0], '#')) {
    return failed; 
  }
  
  struct STRING *buffer = struct_STRING();
  string$from_c_string(buffer, line);
  
  string$shrink_by(buffer, 1);

  if(string$equals(&LABEL_NAME, buffer)) {
    ctx->mode = name;
    return failed;
  }

  if(string$equals(&LABEL_PROGRAM, buffer)) {
    ctx->mode = program_files;
    return failed;
  } 

  if(string$equals(&LABEL_EXTERNAL, buffer)) {
    ctx->mode = external;
    return failed;
  }

/*
  if(string$equals(LABEL_TEST, &buffer)) {
    ctx->mode = test_files;
    return true;
  }

  if(string$equals(LABEL_PROGRAM, &buffer)) {
    ctx->mode = program_files;
    return true;
  }

  if(ctx->mode == no_mode) {
    // @todo set error type
    return false;
  }
*/
  if(ctx->mode == program_files) { 

    if(string$len(buffer) > 0) {
      list$append(ctx->program_files, buffer);
    }

    return failed;
  }

  if(ctx->mode == name) {

    ctx->name     = buffer;
    ctx->mode     = no_mode;

    ctx->has_name = true;
    return failed;
  } 

  if(ctx->mode == external) { 
    if(string$len(buffer) > 0) {

      if(string$chars_start_with(line, "local")) {
        struct EXTERNAL *ext;
	ext = struct_EXTERNAL();
	u32 buff_len = string$len(buffer); 
        
	struct STRING *ref = struct_STRING(); 
	string$copy_chars(ref, line, 6, buff_len - 6);

	ext->ext_type = local; 
	ext->reference = ref; 

        list$append(ctx->external, ext);
	return failed;

      } else if(string$chars_start_with(line, "git")) {
	if(word_count != 3) {
          printf("ERROR: cannot pass wax file, external git reference needs 3 items");
	  failed = true;
	  return failed;
	}
        struct EXTERNAL *ext;
	ext = struct_EXTERNAL(); 

	ext->ext_type  = git; 
	ext->reference = words[1]; 
	ext->version   = words[2]; 

        list$append(ctx->external, ext); 

	return failed;
      } else if(string$chars_start_with(line, "git_perm")) {
	if(word_count != 3) {
          printf("ERROR: cannot pass wax file, external git reference needs 3 items");
	  failed = true;
	  return failed;
	}
        struct EXTERNAL *ext;
	ext = struct_EXTERNAL(); 

	ext->ext_type  = git_perm; 
	ext->reference = words[1]; 
	ext->version   = words[2]; 

        list$append(ctx->external, ext); 

	return failed;
      }
    } 
    return failed;
  } 
  return failed;
/*
  if(ctx->mode ==test_files) {
    list$append(ctx->test_files, &buffer);
    // @todo check file exists
    return true;
  }

  if(ctx->mode == program_files) {
    list$append(ctx->program_files, &buffer);
    // @todo check file exists
    return true;
  }

  // @todo set error type
  return false;
  */
}


bool global_config_handle_line(struct CTX *ctx, char *line) {

  if(strlen(line) == 0) {
    return true;
  } 
  
  struct STRING *buffer = struct_STRING();
  string$from_c_string(buffer, line);
  
  string$shrink_by(buffer, 1);

  if(string$equals(&LABEL_LOCAL, buffer)) {
    ctx->config_mode = config_local;
    return true;
  }

  if(ctx->config_mode == config_local) {

    if(string$len(buffer) > 0) {
      list$append(ctx->config_local, buffer);
    }

    return true;
  }

  return true;
}



/*

bool file_input_handle_line(struct CTX *ctx, 
		            struct FILE_INFO *file_info, 
			    char *line) {

  // increment file line count
  // increment out file line count 
  
  // @todo
  return true;
}
*/
enum  stats_read_mode { read_file_name, read_file_line_count };

bool file_stats_handle_line(struct CTX *ctx, char *line, 
		            enum stats_read_mode toggle) {
  // @todo 
  struct FILE_INFO *file_info;


  if(toggle == read_file_name) {
    // create and set file info
    return true;
  }
  if(toggle == read_file_line_count) {
    // create and set file info
    return true;
  }
  // 
  return true;

}

/*
bool error_handle_line(struct CTX *ctx, struct FILE_STATS *file_stats) {
  // check to see if the contains errors
  // check to see if the errors are to do with one of our project files
  // replace the error line with 
  //
  return false;
}
*/

s32 process_file(struct CTX *ctx, FILE *file_handle) {

  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  bool failed;

  struct STRING *words[10];
  u32    word_count;

  while ((read = getline(&line, &len, file_handle)) != -1) { 
    word_count = string$split_to_array(line, words);
    failed = config_handle_line(ctx, line, words, word_count);
    if(failed == true) {
      exit(1); // TODO horrible error handling, fix so it bubbles up
    }
  }

  return 1;
}



s32 read_config(struct CTX *ctx, char *path) { 
  // TODO error handling when path isn't found 

  // file open
  FILE *file_handle = fopen(path, "r"); 

  if(file_handle == NULL) {
    printf("FILE NOT FOUND, read_config: %s", path);
    return -1;
  } 

  // validate file path 
  process_file(ctx, file_handle);
  	
  // file close	
  fclose(file_handle);

  return 0;
}


s32 process_file_global_config(struct CTX *ctx, FILE *file_handle) {

  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  while ((read = getline(&line, &len, file_handle)) != -1) {
    global_config_handle_line(ctx, line);
  }

  return 1;
}


s32 read_global_config(struct CTX *ctx) {
  char* user_home = getenv("HOME");
  struct STRING cfg_path;

  string$from_c_string(&cfg_path, user_home); 
  string$append_chars(&cfg_path, "/.wax.cfg"); 
  
  // file open
  FILE *file_handle = fopen(string$to_c_string(&cfg_path), "r"); 
  if(file_handle == NULL) {
    printf("global config not available: %s ", string$to_c_string(&cfg_path));
    return -1;
  }

  // validate file path 
  process_file_global_config(ctx, file_handle);
  	
  // file close	
  fclose(file_handle); 

  return 0;
}


struct FILE_INDEX {
  struct
  STRING *file_name;

  u64  offset;
};


void init_FILE_INDEX(struct FILE_INDEX *file_index) {
  file_index->offset = 0;
}


struct FILE_INDEX *make_FILE_INDEX() {
  struct FILE_INDEX *file_index;

  file_index = malloc(sizeof(struct STRING));

  init_FILE_INDEX(file_index);

  return file_index;  
}


// TODO Deprecate get_c_file_path
void get_c_file_path(struct STRING *source_name) {
   char *prefix    = "src/";
   char *suffix    = ".c";
   char *seperator = "/";
   
   u32  source_name_len = string$len(source_name);
   u32  prefix_len      = strlen(prefix);

   string$insert_chars(source_name, prefix); 

   string$append_chars(source_name, suffix); 
} 


bool handle_main_case(char *line) {
  // what is the method of excluding main, cause what happens if it has a bug in it??  
  
  char *main_call = "int main(int argc, char **argv)";

  if(string$chars_start_with_ignore_white_space(line, main_call) >= 0) {
    return true;
  } 
  return false; 
}


struct TEST {
  struct STRING *test_function;
  struct STRING *test_name;
  struct STRING *module;
};


struct TEST *struct_TEST() {
  struct TEST *test;

  test = malloc(sizeof(struct TEST));

  return test;
}


void handle_test_case(struct CTX    *ctx, 
		      char          *line, 
		      char          *module_prefix_chars,
		      struct STRING *module) {


  s32 index = string$chars_start_with_ignore_white_space(line, module_prefix_chars); 

  if(index >= 0) {
    // printf("@@@@@@@@@@@@@@@@@@ %s @@@@@@@@@@@@@@@@@@\n", line);

    u32 end_of_function_name = string$chars_index_of_from(line, '(', index);

    // printf("found at: %d\n index: %d\n", end_of_function_name, index);

    struct TEST   *test_e         = struct_TEST(); 
    struct STRING *test_function  = struct_STRING(); 
    struct STRING *test_name      = struct_STRING(); 


    string$init(test_function);
    string$append_chars_section(test_function, 
		                index + 4, 
				end_of_function_name - index, 
				line);
    
    // NOTE: theoritically should always find $
    s32 module_end = string$chars_index_of(line, '$');

    // printf("############### module_end: %d, function_end %d\n", module_end, end_of_function_name);

    string$copy_chars(test_name, 
		      line, module_end + 1 , 
		      end_of_function_name - module_end - 1);

    u32  padding =  28 + string$len(test_name) + string$len(module);

    if(padding > ctx->test_padding) {
      ctx->test_padding = padding;
    }

    //1451Gstring$ 

    test_e->test_function = test_function;
    test_e->module        = module;
    test_e->test_name     = test_name; 
/*
    printf("test function: %s, module: %s, test_name: %s\n",
	   string$to_c_string(test_e->test_function),   
           string$to_c_string(test_e->module), 
	   string$to_c_string(test_e->test_name));
*/
    list$append(ctx->tests, test_e);

  } 
}


void get_c_module_test_prefix(struct STRING *module, 
                              struct STRING *test_prefix) {

  string$init(test_prefix);  
  
  // string$append_chars(test_prefix, "bool ");
  string$append_chars(test_prefix, "u32 ");
  string$append(test_prefix, module);
  string$append_chars(test_prefix, "_test"); 

} 


u32 append_source_file_test(struct CTX *origin_ctx,
		            struct CTX *ctx,
		            struct STRING *module, 
		            struct STRING *module_path, 
			    FILE   *super_file) { 

  struct STRING test_prefix; 
  u32  line_count   = 0; 

  get_c_module_test_prefix(module, &test_prefix);

  char *module_prefix_chars = string$to_c_string(&test_prefix); 

  FILE *module_file = file$open_r(string$to_c_string(module_path));

  char *line   = NULL;
  size_t len   = 10;
  ssize_t read;

  while ((read = getline(&line, &len, module_file)) != -1) { 
    // printf("num: %d\n", line_count);
    if(handle_main_case(line)) { 
      fprintf(super_file, "int hidden_main(int argc, char **argv) {\n");
    } else {
      //handle_test_case(ctx, line, module_prefix_chars, module);
      handle_test_case(origin_ctx, line, module_prefix_chars, module);
      fprintf(super_file, "%s",line); 
    }
    line_count ++;
  }
  file$close(module_file);

  return line_count;
}


u32 append_source_file(struct CTX *ctx,
		       struct STRING *module, 
		       struct STRING *module_path, 
		       FILE   *super_file) { 

  struct STRING test_prefix; 
  u32  line_count   = 0; 

  get_c_module_test_prefix(module, &test_prefix);

  char *module_prefix_chars = string$to_c_string(&test_prefix); 

  FILE *module_file = file$open_r(string$to_c_string(module_path));

  char *line   = NULL;
  size_t len   = 10;
  ssize_t read;

  while ((read = getline(&line, &len, module_file)) != -1) { 
    // printf("num: %d\n", line_count);
    fprintf(super_file, "%s",line); 
    line_count ++;
  }
  file$close(module_file);

  return line_count;
}


void get_build_info_path(struct CTX *ctx, struct STRING *path) {
  char *chars_base_path = "x/";
  char *chars_info_suffix = "_build.info";

  string$from_c_string(path, chars_base_path); 
  string$append(path, ctx->name); 
  string$append_chars(path, chars_info_suffix);
}


void append_tests_and_main(struct CTX *ctx, FILE *super_file) {
  // write the run test function 
  fprintf(super_file, "\n\nu32 run_tests() {\n");
  fprintf(super_file, "  u32 total_failed = 0;\n");
  fprintf(super_file, "  u32 failed_tests = 0;\n");
  
  // write the run text end 
  struct LIST *tests = ctx->tests;
  if(!list$empty(tests)) { 

    struct LIST_ITEM *list_item;
    struct STRING    *test_call;
  
    list_item = list$first(tests);  
    struct TEST *test;

    struct STRING test_label; 

    fprintf(super_file, "  printf(\"Running tests: \\n\\n\");");

    do { 
      test = list$get(list_item);

      string$init(&test_label);
      string$append_chars(&test_label, "  printf(\"[");
      string$append(&test_label, test->module); 
      string$append_chars(&test_label, "] "); 
      string$append(&test_label, test->test_name);
      //printf("############ %d ##############", ctx->test_padding);
      string$append_padding(&test_label, ' ', ctx->test_padding);
      string$append_chars(&test_label, "\");\n"); 

      fprintf(super_file, "\n%s", string$to_c_string(&test_label));


      fprintf(super_file, "  failed_tests = %s();\n", string$to_c_string(test->test_function)); 

      fprintf(super_file, "  if(failed_tests > 0) {\n");
      fprintf(super_file, "    printf(\"Failed\\n\");\n");
      fprintf(super_file, "  } else {\n");
      fprintf(super_file, "    printf(\"Success\\n\");\n");
      fprintf(super_file, "  }\n");
      fprintf(super_file, "  total_failed += failed_tests;\n"); 
		
      list_item = list$next(list_item);
    } while(list$has_next(list_item));
  } 
  fprintf(super_file, "  return failed_tests;\n");
  fprintf(super_file, "}\n");

  // write the main function
  fprintf(super_file, "\n\n");
  fprintf(super_file, "int main(int argc, char **argv) {\n");
  fprintf(super_file, "  u32 failed_tests;\n"); 
  fprintf(super_file, "  failed_tests = run_tests();\n\n");
  fprintf(super_file, "  if(failed_tests > 0) { \n"); 
  fprintf(super_file, "    printf(\"\\nTests failed: %%d\\n\\n\", failed_tests);\n");

  fprintf(super_file, "    return -1;\n");
  fprintf(super_file, "  } else {");
  fprintf(super_file, "\n    printf(\"\\nSuccess!\\n\\n\");\n"); 
  fprintf(super_file, "    return 0;\n"); 
  fprintf(super_file, "  }");
  fprintf(super_file, "\n}\n");

}

void expand_home(struct STRING *path) {
  // TODO: check if path isn't empty
  
  if(string$char_at(path, 0) == '~') {
    char *c_home = getenv("HOME");
    struct STRING home;

    string$from_c_string(&home, c_home);

     string$replace(path, 0, 1, &home); 
  } 
}

// ZZ

void git_name(struct STRING   *name,
              struct EXTERNAL *ext) {

  u32 index  = string$last_index_of_char(ext->reference, '/'); 
  u32 amount = string$len(ext->reference); 

  amount = amount - index - 5; 

  string$copy_string(name, ext->reference, index + 1, amount); 
}


void get_external_base_path(struct CTX      *origin_ctx, 
		            struct EXTERNAL *ext,
			    struct STRING   *external_base_path) {

 
  // handle git externals
  if(ext->ext_type == git || ext->ext_type == git_perm) {
    // string$from_c_string(external_base_path, "remote_src/");

    if(string$len(external_base_path) > 0) { 
      string$append_chars(external_base_path, "/");
    }
    string$append_chars(external_base_path, "remote_src/");

    struct STRING name; // TODO we could make name as part of the external struct
    string$init(&name);
    git_name(&name, ext);

    string$append(external_base_path, &name);
    return;
  }

  // handle local externals
  // TODO TODO TODO string$init(external_base_path);  TODO TODO TODO
  if(!list$empty(origin_ctx->config_local)) {
    struct LIST_ITEM *li       = list$first(origin_ctx->config_local);
    struct STRING *local_base  = list$get(li); 

    string$copy(local_base, external_base_path);

  } else {
    printf("ERROR: local base directory not set!\n");
    exit(1);
  }

  expand_home(external_base_path);
 
  string$append_chars(external_base_path, "/");
  string$append(external_base_path, ext->reference); 
} 


void get_external_ctx_path(struct EXTERNAL *ext,
		           struct STRING   *external_base_path, 
			   struct STRING   *external_ctx_path ) {

  string$init(external_ctx_path);

  string$copy(external_base_path, external_ctx_path);
  string$append_chars(external_ctx_path, "/"); 

  if(ext->ext_type == git || ext->ext_type == git_perm) {
    struct STRING name;
    string$init(&name);
    git_name(&name, ext);

    string$append(external_ctx_path, &name); 
  }
  if(ext->ext_type == local) {
    string$append(external_ctx_path, ext->reference);
  }
  string$append_chars(external_ctx_path, ".wax"); 
}


void get_super_file_path_test(struct CTX    *ctx,
		              struct STRING *super_file_path ) {

  string$init(super_file_path);

  // create the file path
  char *chars_base_path = "x/";
  char *chars_suffix = "_super_test.c";

  string$append_chars(super_file_path, chars_base_path); 
  string$append(super_file_path, ctx->name); 
  string$append_chars(super_file_path, chars_suffix); 
}


void get_super_file_path(struct CTX    *ctx,
		         struct STRING *super_file_path ) {

  string$init(super_file_path);

  // create the file path
  char *chars_base_path = "x/";
  char *chars_suffix = "_super.c";

  string$append_chars(super_file_path, chars_base_path); 
  string$append(super_file_path, ctx->name); 
  string$append_chars(super_file_path, chars_suffix); 
}


void get_file_path(struct CTX    *ctx, 
		   struct STRING *base_path, 
		   struct STRING *module,
		   struct STRING *file_path) {

   string$init(file_path);

   // TODO in future use ctx to check for file type
   char *prefix    = "src/";
   char *suffix    = ".c";
   char *seperator = "/";

   string$append(file_path, base_path);

   if(string$len(file_path) > 0) {
     string$append_chars(file_path, seperator);
   } 

   string$append_chars(file_path, prefix);
   string$append(file_path, module);
   string$append_chars(file_path, suffix);
} 


// TODO Change input for this function
void build_super_source_file_test(struct CTX    *origin_ctx, 
				  struct CTX    *sub_ctx,
				  struct STRING *base_path) { 
  struct CTX *ctx; 
  bool is_origin;

  if(sub_ctx == NULL) {
    ctx = origin_ctx;
    is_origin = true;
  } else {
    ctx = sub_ctx;
    is_origin = false;
  }

  FILE *super_file;
  FILE *info_file;

  if(is_origin == true) {
    struct STRING super_file_path;
    get_super_file_path_test(origin_ctx, &super_file_path);

    struct STRING build_info_path;
    get_build_info_path(origin_ctx, &build_info_path); 

    super_file = file$open_w(string$to_c_string(&super_file_path)); 
    info_file  = file$open_w(string$to_c_string(&build_info_path)); 

    origin_ctx->super_file = super_file;
    origin_ctx->info_file  = info_file; 

  } else { 

    super_file = origin_ctx->super_file;
    info_file  = origin_ctx->info_file; 
  } 

  struct LIST       *list_external; 
  struct EXTERNAL   *ext;

  struct STRING     external_base_path;
  struct STRING     external_ctx_path;

  struct CTX        *sub_sub_ctx;
  struct LIST_ITEM  *li;

  list_external = ctx->external;
  
  // Process the externals in this source file
  if(!list$empty(list_external)) {
  
    li = list$first(list_external);
    do {
      ext = list$get(li); 

      string$init(&external_base_path);
      string$append(&external_base_path, base_path);

      printf("~~~~~~~~~~ ext base path: %s", string$to_c_string(&external_base_path));

      get_external_base_path(origin_ctx, ext, &external_base_path);
      printf("here 1\n");
      get_external_ctx_path(ext, &external_base_path, &external_ctx_path); 
      printf("here 2\n");
     
      sub_sub_ctx = struct_CTX(); 
      printf("reading sub_ctx\n");
      read_config(sub_sub_ctx, string$to_c_string(&external_ctx_path)); 

      printf("building super sub_ctx\n");
      build_super_source_file_test(origin_ctx, sub_sub_ctx, &external_base_path);
      free(sub_sub_ctx);
      

      li = list$next(li);
    } while(list$has_next(li)); 
  } 

  struct LIST *program_files = ctx->program_files; 
  if(list$empty(program_files)) {
     return;
  } 

  // Process the files in this build 
  li = list$first(program_files);

  struct STRING file_path;
  struct STRING *module;
  u32 file_line_count = 0; 

  li = list$first(program_files);
  do { 
    module = list$get(li); 
    
    get_file_path(ctx, base_path, module, &file_path);

    file_line_count = append_source_file_test(origin_ctx, ctx, module, &file_path, super_file);
    
    fprintf(info_file, "%s\t%d\n", string$to_c_string(&file_path), file_line_count); 
    
    ctx->total_line_count += file_line_count; 

    li = list$next(li);
  } while(list$has_next(li)); 


  // append tests and main test
  if(is_origin) {
    append_tests_and_main(ctx, super_file); 

    file$close(super_file);
    file$close(info_file);
  } 
}


void create_build_info_path(struct STRING *name, struct STRING *path) {
  char *chars_base_path = "x/";
  char *chars_info_suffix = "_build.info";

  string$from_c_string(path, chars_base_path); 
  string$append(path, name); 
  string$append_chars(path, chars_info_suffix);
}


void build_super_source_file(struct CTX    *origin_ctx, 
			     struct CTX    *sub_ctx,
			     struct STRING *base_path) { 

  struct CTX *ctx; 
  bool is_origin;

  if(sub_ctx == NULL) {
    ctx = origin_ctx;
    is_origin = true;
  } else {
    ctx = sub_ctx;
    is_origin = false;
  } 

  FILE *super_file;
  FILE *info_file;

  if(is_origin == true) {
    struct STRING super_file_path;
    get_super_file_path(origin_ctx, &super_file_path);

    struct STRING build_info_path;
    get_build_info_path(origin_ctx, &build_info_path); 

    super_file = file$open_w(string$to_c_string(&super_file_path)); 
    info_file  = file$open_w(string$to_c_string(&build_info_path)); 

    origin_ctx->super_file = super_file;
    origin_ctx->info_file  = info_file; 

  } else { 

    super_file = origin_ctx->super_file;
    info_file  = origin_ctx->info_file; 
  } 

  struct LIST       *list_external; 
  struct EXTERNAL   *ext;

  struct STRING     external_base_path;
  struct STRING     external_ctx_path;

  struct CTX        *sub_sub_ctx;
  struct LIST_ITEM  *li;

  list_external = ctx->external;
  
  // Process the externals in this source file
  if(!list$empty(list_external)) {
  
    li = list$first(list_external);
    do {
      ext = list$get(li); 

      string$init(&external_base_path);
      string$append(&external_base_path, base_path); 

      get_external_base_path(origin_ctx, ext, &external_base_path);
      get_external_ctx_path(ext, &external_base_path, &external_ctx_path); 
    
      sub_sub_ctx = struct_CTX(); 

      read_config(sub_sub_ctx, string$to_c_string(&external_ctx_path)); 
      build_super_source_file(origin_ctx, sub_sub_ctx, &external_base_path);

      li = list$next(li);
    } while(list$has_next(li)); 
  } 

  struct LIST *program_files = ctx->program_files; 
  if(list$empty(program_files)) {
     return;
  } 

  // Process the files in this build 
  li = list$first(program_files);

  struct STRING file_path;
  struct STRING *module;
  u32 file_line_count = 0; 

  li = list$first(program_files);

  do { 
    module = list$get(li); 

    get_file_path(ctx, base_path, module, &file_path);

    file_line_count = append_source_file(ctx, module, &file_path, super_file);
    
    fprintf(info_file, "%s\t%d\n", string$to_c_string(&file_path), file_line_count); 
    
    ctx->total_line_count += file_line_count; 

    li = list$next(li);
  } while(list$has_next(li)); 

  // close the file
  if(is_origin) { 
    file$close(super_file);
    file$close(info_file);
  }
} 



void load_build_info_line(struct BUILD_INFO *build_info, char *line, s32 index_of_tab) {
  struct STRING *module;
  module = struct_STRING();

  u32           line_count = 0; 

  string$from_c_string_limit(module, line, index_of_tab);
  line_count = string$chars_to_u32(line, index_of_tab + 1);

  build_info->module     = module;
  build_info->line_count = line_count; 
}



void load_build_info(struct CTX *ctx) {
  // create the path
  struct STRING path;
  create_build_info_path(ctx->name, &path);
 
  // open the file
  FILE *info_file = file$open_r(string$to_c_string(&path));

  printf("opening file %s\n", string$to_c_string(&path));
 
  // make the list and add it to the context
  struct LIST *info_list = list$make();
  ctx->build_info = info_list;

  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  struct BUILD_INFO *build_info;
  s32 index_of_tab = -1;

  // read each line
  while ((read = getline(&line, &len, info_file)) != -1) {

   // split on the tab
   index_of_tab = string$chars_index_of(line, '\t');

   if(index_of_tab > 0) {
      build_info = struct_BUILD_INFO();
      load_build_info_line(build_info, line, index_of_tab);

      // add to list
      list$append(info_list, build_info);
    } 
  } 
 
  // close the file
  file$close(info_file); 
}


void print_build_info(struct CTX *ctx) {
  struct LIST *build_info; 
  build_info = ctx->build_info;

  struct BUILD_INFO *info;

  if(list$empty(build_info)) {
    printf("this is empty :-(\n");
    return;
  }

  struct LIST_ITEM *li;
  li = list$first(build_info);

  do {
   info = list$get(li);
   
   printf("file: %s line_count: %d\n", string$to_c_string(info->module), info->line_count);
   li = list$next(li);

  } while(list$has_next(li)); 
}


bool translate_line_no(struct LIST *info_list, u32 line_no, struct BUILD_INFO *result) {
  if(list$empty(info_list)) {
    return false;
  }

  u32 counted = 0;
  struct BUILD_INFO *found = NULL;
  struct LIST_ITEM *li = list$first(info_list);

  do {
    found = list$get(li);

    if(found->line_count + counted > line_no) {

      result->module     = found->module;
      result->line_count = line_no - counted;
      
      return true;
    }

    counted += found->line_count;
	     
    li = list$next(li);
  } while(list$has_next(li));

  return false;
}


void term_file() {
  printf("\033[1;315m");
}


void term_reset() {
  printf("\033[0m");
}


void handle_compiler_output(struct CTX *ctx, bool test_mode) {
  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  // QQ
  // TODO: get super file name
  struct STRING super_file_path;
  if(test_mode) {
    get_super_file_path_test(ctx, &super_file_path);
  } else {
    get_super_file_path(ctx, &super_file_path);
  }
  
  char *super_file       = string$to_c_string(&super_file_path);
  u32  super_file_len    = strlen(super_file);
  struct LIST *info_list = ctx->build_info;
  bool found             = false;

  u32  line_no;
  struct BUILD_INFO result;

  struct STRING new_line_no;

  char* blank_c = "";
  struct STRING blank;
  string$from_c_string(&blank, blank_c);

  while ((read = getline(&line, &len, stdin)) != -1) {
    if(string$chars_start_with(line, super_file)) {
       line_no = string$chars_to_u32(line, super_file_len + 1);   

       found = translate_line_no(info_list, line_no, &result);

       if(found) {
	 struct STRING line_string;
         string$from_c_string(&line_string, line);

	 // replace the file no 
	 string$u32_to_string(&new_line_no, result.line_count);
 
	 u32 replace_count = string$chars_index_of_from(line, ':', super_file_len + 1);
	 replace_count -= super_file_len; 

	 // printf("************* REPLACE COUNT %d *************\n", replace_count);

	 
	 string$replace(&line_string, super_file_len + 1, replace_count - 1, &new_line_no); 

         // replace the file name
	 //string$replace(&line_string, 0, super_file_len, result.module); 
         string$replace(&line_string, 0, super_file_len, &blank); 

	 // print
	 term_file();
	 printf("%s", string$to_c_string(result.module)); 
	 term_reset();

	 printf("%s", string$to_c_string(&line_string));
	 
       } else {
         printf("NOT%s", line); // TODO: BUG what do we do if we don't find anything?  
       } 
    } else {
      printf("%s", line);
    } 
  } 
}

/*
  if(argc == 3 && strcmp(argv[2],"-t") == 0) {
    printf("Buiding super source test\n");
    build_sub_sources_test(ctx);
    build_super_source_file_test(ctx);

    return 0;
  }

  printf("Buiding super source\n");
  build_sub_sources(ctx);
  build_super_source_file(ctx);
*/ 


u32 write_deps(struct CTX *ctx) { 
  struct LIST *external;
  external = ctx->external; 

  if(list$empty(external) == true) {
    return 0;
  } 

  // open the write to write
  FILE  *deps_file = file$open_w("x/remote.deps");

  if(deps_file == NULL) {
    printf("INTERNAL ERROR not able to create: x/remotes.deps\n");
    return 1;
  }

  struct LIST_ITEM *li = list$first(external);
  struct EXTERNAL  *ext;
  struct STRING    git_clone_cmd; 
  struct STRING    name; 
  char*            remote_dir  = "remote_src/";
  struct STRING    path;

  string$init(&git_clone_cmd);
  string$init(&name);

  do {  

    ext = list$get(li);
    if(ext->ext_type == git || ext->ext_type == git_perm) {
      // TODO handle failure of last_index_of
      /*
      u32 index  = string$last_index_of_char(ext->reference, '/'); 

      u32 amount = string$len(ext->reference); 
      amount = amount - index - 5;

      string$clear(&name); 
      string$copy_string(&name, ext->reference, index + 1, amount); 
      */
      string$clear(&name); 
      git_name(&name, ext);

      string$from_c_string(&path, remote_dir); 
      string$append(&path, &name); 
  
      bool exists   = file$exists(string$to_c_string(&path));
      bool updating = false;

      // exists but is the master branch we want to update it
      if(exists == true && string$equals_chars(ext->version, "master") == true) {
        
        string$append_chars(&git_clone_cmd, "git -C remote_src/"); 
        string$append(&git_clone_cmd, &name); 
        string$append_chars(&git_clone_cmd, " pull"); 

        // write the reference to the file
        printf("%s\n", string$to_c_string(&git_clone_cmd)); 
        fprintf(deps_file, "%s\n", string$to_c_string(&git_clone_cmd));

	updating = true;
      }
      // see if the branch has already been retrieved
      if(exists == false) {
        // append the git clone prefix
        string$append_chars(&git_clone_cmd, "git -C remote_src"); 

        // append the tag/branch
        string$append_chars(&git_clone_cmd, " clone --branch "); 
        string$append(&git_clone_cmd, ext->version); 
        string$append_chars(&git_clone_cmd, " --single-branch --depth 1 "); 

        // append the repo 
        string$append(&git_clone_cmd, ext->reference); 

        // write the reference to the file
        printf("%s\n", string$to_c_string(&git_clone_cmd));
        fprintf(deps_file, "%s\n", string$to_c_string(&git_clone_cmd));

	updating = true;
      }

      if(updating == true) {
        fprintf(deps_file, "git_add remote_src/%s\n", string$to_c_string(&name));

      } 

      // run deps on the deps to make sure sub dependencies are updated
      fprintf(deps_file, "(cd remote_src/%s && deps)\n", string$to_c_string(&name));

      // reset the string for the next line
      string$clear(&git_clone_cmd); 
    }

    li = list$next(li);

  } while(list$has_next(li));

  file$close(deps_file);

  return 0;
}


// ALL THE TESTS ---------------


u32 wax_test$get_c_file_path() { 
  char *c_file_path = "hello";
  struct STRING file_path;
  string$from_c_string(&file_path, c_file_path);

  char *c_result = "src/hello.c";
  struct STRING result;
  string$from_c_string(&result, c_result);

  get_c_file_path(&file_path);

  if(!string$equals(&file_path, &result)) {
    printf("***FAILED*** result no expected, got: ");   
    printf("[");   
    print(&file_path);
    printf("]");   
    printf(" instead of [src/hello/hello.c]\n");
    return 1; 
  }

  return 0;
} 


void setup() {
  string$from_c_string(&LABEL_NAME,     C_LABEL_NAME);
  string$from_c_string(&LABEL_TEST,     C_LABEL_TEST);
  string$from_c_string(&LABEL_PROGRAM,  C_LABEL_PROGRAM);
  string$from_c_string(&LABEL_LOCAL,    C_LABEL_LOCAL);
  string$from_c_string(&LABEL_EXTERNAL, C_LABEL_EXTERNAL);
}


int main(int argc, char **argv) { 
  if(argc < 2) {
    printf("expected: wax file.\n");
    return -1;
  }

  setup(); 

  struct CTX *ctx; 
  ctx = struct_CTX();

  // read global config
  read_global_config(ctx); 
  read_config(ctx, argv[1]);

  // TODO validate config file
  //
  // print_CTX(ctx); 
  if(argc == 3 && strcmp(argv[2],"-deps") == 0) {
    write_deps(ctx);
    return 0;
  }

  if(argc == 3 && strcmp(argv[2],"-ot") == 0) {
    printf("Handling compiler output!\n");
    load_build_info(ctx);
    print_build_info(ctx);

    if(ftell(stdin) < 0) {
      handle_compiler_output(ctx, true);
      return -1;
    }

    return 0;
  }

  if(argc == 3 && strcmp(argv[2],"-o") == 0) {
    printf("Handling compiler output!\n");
    load_build_info(ctx);
    print_build_info(ctx);

    if(ftell(stdin) < 0) {
      handle_compiler_output(ctx, false);
      return -1;
    }

    return 0;
  }

  struct STRING base_path; 
  string$init(&base_path);
  if(argc == 3 && strcmp(argv[2],"-t") == 0) { 

    build_super_source_file_test(ctx, NULL, &base_path); 
    return 0;
  }

  printf("Buiding super source\n");
  build_super_source_file(ctx, NULL, &base_path);

  return 0;
} 
