/*
 
   Copyright 2019 RNGHACK

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/ 

/*

   TODO

   BUG: extra spaces at the end of config line
   BUG: extra lines  at the end of config file
*/


// WAX:main ----------------

char* C_LABEL_NAME     = "name:";
char* C_LABEL_TEST     = "test:";
char* C_LABEL_PROGRAM  = "program:";
char* C_LABEL_LOCAL    = "local:";
char* C_LABEL_EXTERNAL = "external:";


struct  STRING LABEL_NAME;
struct  STRING LABEL_TEST;
struct  STRING LABEL_PROGRAM;
struct  STRING LABEL_LOCAL;
struct  STRING LABEL_EXTERNAL;

enum mode_type         { no_mode, name, program_files, external };
enum config_mode_type  { config_no_mode, config_local };

struct CTX {
  bool            has_name;
  bool            has_program;

  enum mode_type         mode;
  enum config_mode_type  config_mode;

  struct STRING   *name;
  
  struct LIST     *test_files; 
  struct LIST     *program_files; 
  struct LIST     *build_info; 

  struct LIST     *config_local;
  struct LIST     *external;

  struct LIST     *tests; 

  u32             test_padding;
  u32             total_line_count;

  FILE            *super_file; 
  FILE            *info_file; 

};


enum external_type { local, git, git_perm };

struct EXTERNAL {
  enum   external_type  ext_type;
  struct STRING         *reference;
  struct STRING         *version;
};

struct EXTERNAL *struct_EXTERNAL() {
  struct EXTERNAL *external;

  external = malloc(sizeof(struct EXTERNAL)); 

  return external; 
}


void print_CTX(struct CTX *ctx) {
  struct STRING    *p_str;
  struct EXTERNAL  *ext;
  struct LIST      *list;
  u32 count;

  printf("CTX {\n  name : ");
  print(ctx->name); 

  // config local
  list = ctx->config_local;
  count = 0;
  printf("\n  config local: ["); 

  if(!list$empty(list)) {
    struct LIST_ITEM *li = list$first(list);
    do {
       p_str = list$get(li);
       print(p_str);
       printf(" ");

      li = list$next(li);
      count++;
    } while(list$has_next(li));
  }
  printf("]");


  // external files
  list = ctx->external;
  count = 0;
  printf("\n  external: ["); 

  if(!list$empty(list)) {
    struct LIST_ITEM *li = list$first(list);
    do {
       ext = list$get(li);
       print(ext->reference);
       printf(" ");

      li = list$next(li);
      count++;
    } while(list$has_next(li));
  }
  printf("]");


  // program files
  list = ctx->program_files;
  count = 0;
  printf("\n  program_files: ["); 

  if(!list$empty(list)) {
    struct LIST_ITEM *li = list$first(list);
    do {
       p_str = list$get(li);
       print(p_str);
       printf(" ");

      li = list$next(li);
      count++;
    } while(list$has_next(li));
  }
  printf("]");



  printf("}\n");  
}


struct CTX *struct_CTX() {
  struct CTX *ctx;

  ctx = malloc(sizeof(struct CTX));

  ctx->has_name = false;
  ctx->has_program = false;

  ctx->mode = no_mode;

  ctx->program_files = list$make();
  ctx->tests = list$make();
  ctx->config_local = list$make();
  ctx->external = list$make();

  ctx->test_padding = 0; 
  ctx->total_line_count = 0;

  return ctx;
}


struct BUILD_INFO {
  struct STRING *module;
  u32           line_count; 
}; 


struct BUILD_INFO *struct_BUILD_INFO() {
  struct BUILD_INFO *build_info;

  build_info = malloc(sizeof(struct BUILD_INFO));

  build_info->line_count = 0;

  return build_info;
};


// TODO convert everything to use words array
bool config_handle_line(struct CTX    *ctx, 
                        char          *line, 
			struct STRING *words[],
			u32           word_count) {

  bool failed = false;
  if(word_count == 0) {
    return failed;
  } 
  // skip line if comment
  if(string$start_with_char(words[0], '#')) {
    return failed; 
  }
  
  struct STRING *buffer = struct_STRING();
  string$from_c_string(buffer, line);
  
  string$shrink_by(buffer, 1);

  if(string$equals(&LABEL_NAME, buffer)) {
    ctx->mode = name;
    return failed;
  }

  if(string$equals(&LABEL_PROGRAM, buffer)) {
    ctx->mode = program_files;
    return failed;
  } 

  if(string$equals(&LABEL_EXTERNAL, buffer)) {
    ctx->mode = external;
    return failed;
  }

/*
  if(string$equals(LABEL_TEST, &buffer)) {
    ctx->mode = test_files;
    return true;
  }

  if(string$equals(LABEL_PROGRAM, &buffer)) {
    ctx->mode = program_files;
    return true;
  }

  if(ctx->mode == no_mode) {
    // @todo set error type
    return false;
  }
*/
  if(ctx->mode == program_files) { 

    if(string$len(buffer) > 0) {
      list$append(ctx->program_files, buffer);
    }

    return failed;
  }

  if(ctx->mode == name) {

    ctx->name     = buffer;
    ctx->mode     = no_mode;

    ctx->has_name = true;
    return failed;
  } 

  if(ctx->mode == external) { 
    if(string$len(buffer) > 0) {

      if(string$chars_start_with(line, "local")) {
        struct EXTERNAL *ext;
	ext = struct_EXTERNAL();
	u32 buff_len = string$len(buffer); 
        
	struct STRING *ref = struct_STRING(); 
	string$copy_chars(ref, line, 6, buff_len - 6);

	ext->ext_type = local; 
	ext->reference = ref; 

        list$append(ctx->external, ext);
	return failed;

      } else if(string$chars_start_with(line, "git")) {
	if(word_count != 3) {
          printf("ERROR: cannot pass wax file, external git reference needs 3 items");
	  failed = true;
	  return failed;
	}
        struct EXTERNAL *ext;
	ext = struct_EXTERNAL(); 

	ext->ext_type  = git; 
	ext->reference = words[1]; 
	ext->version   = words[2]; 

        list$append(ctx->external, ext); 

	return failed;
      } else if(string$chars_start_with(line, "git_perm")) {
	if(word_count != 3) {
          printf("ERROR: cannot pass wax file, external git reference needs 3 items");
	  failed = true;
	  return failed;
	}
        struct EXTERNAL *ext;
	ext = struct_EXTERNAL(); 

	ext->ext_type  = git_perm; 
	ext->reference = words[1]; 
	ext->version   = words[2]; 

        list$append(ctx->external, ext); 

	return failed;
      }
    } 
    return failed;
  } 
  return failed;
/*
  if(ctx->mode ==test_files) {
    list$append(ctx->test_files, &buffer);
    // @todo check file exists
    return true;
  }

  if(ctx->mode == program_files) {
    list$append(ctx->program_files, &buffer);
    // @todo check file exists
    return true;
  }

  // @todo set error type
  return false;
  */
}


bool global_config_handle_line(struct CTX *ctx, char *line) {

  if(strlen(line) == 0) {
    return true;
  } 
  
  struct STRING *buffer = struct_STRING();
  string$from_c_string(buffer, line);
  
  string$shrink_by(buffer, 1);

  if(string$equals(&LABEL_LOCAL, buffer)) {
    ctx->config_mode = config_local;
    return true;
  }

  if(ctx->config_mode == config_local) {

    if(string$len(buffer) > 0) {
      list$append(ctx->config_local, buffer);
    }

    return true;
  }

  return true;
}



/*

bool file_input_handle_line(struct CTX *ctx, 
		            struct FILE_INFO *file_info, 
			    char *line) {

  // increment file line count
  // increment out file line count 
  
  // @todo
  return true;
}
*/
enum  stats_read_mode { read_file_name, read_file_line_count };

bool file_stats_handle_line(struct CTX *ctx, char *line, 
		            enum stats_read_mode toggle) {
  // @todo 
  struct FILE_INFO *file_info;


  if(toggle == read_file_name) {
    // create and set file info
    return true;
  }
  if(toggle == read_file_line_count) {
    // create and set file info
    return true;
  }
  // 
  return true;

}

/*
bool error_handle_line(struct CTX *ctx, struct FILE_STATS *file_stats) {
  // check to see if the contains errors
  // check to see if the errors are to do with one of our project files
  // replace the error line with 
  //
  return false;
}
*/

s32 process_file(struct CTX *ctx, FILE *file_handle) {

  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  bool failed;

  struct STRING *words[10];
  u32    word_count;

  while ((read = getline(&line, &len, file_handle)) != -1) { 
    word_count = string$split_to_array(line, words);
    failed = config_handle_line(ctx, line, words, word_count);
    if(failed == true) {
      exit(1); // TODO horrible error handling, fix so it bubbles up
    }
  }

  return 1;
}



s32 read_config(struct CTX *ctx, char *path) { 
  // TODO error handling when path isn't found 

  // file open
  FILE *file_handle = fopen(path, "r"); 

  if(file_handle == NULL) {
    printf("FILE NOT FOUND, read_config: %s", path);
    return -1;
  } 

  // validate file path 
  process_file(ctx, file_handle);
  	
  // file close	
  fclose(file_handle);

  return 0;
}


s32 process_file_global_config(struct CTX *ctx, FILE *file_handle) {

  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  while ((read = getline(&line, &len, file_handle)) != -1) {
    global_config_handle_line(ctx, line);
  }

  return 1;
}


s32 read_global_config(struct CTX *ctx) {
  char* user_home = getenv("HOME");
  struct STRING cfg_path;

  string$from_c_string(&cfg_path, user_home); 
  string$append_chars(&cfg_path, "/.wax.cfg"); 
  
  // file open
  FILE *file_handle = fopen(string$to_c_string(&cfg_path), "r"); 
  if(file_handle == NULL) {
    printf("global config not available: %s ", string$to_c_string(&cfg_path));
    return -1;
  }

  // validate file path 
  process_file_global_config(ctx, file_handle);
  	
  // file close	
  fclose(file_handle); 

  return 0;
}


struct FILE_INDEX {
  struct
  STRING *file_name;

  u64  offset;
};


void init_FILE_INDEX(struct FILE_INDEX *file_index) {
  file_index->offset = 0;
}


struct FILE_INDEX *make_FILE_INDEX() {
  struct FILE_INDEX *file_index;

  file_index = malloc(sizeof(struct STRING));

  init_FILE_INDEX(file_index);

  return file_index;  
}


// TODO Deprecate get_c_file_path
void get_c_file_path(struct STRING *source_name) {
   char *prefix    = "src/";
   char *suffix    = ".c";
   char *seperator = "/";
   
   u32  source_name_len = string$len(source_name);
   u32  prefix_len      = strlen(prefix);

   string$insert_chars(source_name, prefix); 

   string$append_chars(source_name, suffix); 
} 


bool handle_main_case(char *line) {
  // what is the method of excluding main, cause what happens if it has a bug in it??  
  
  char *main_call = "int main(int argc, char **argv)";

  if(string$chars_start_with_ignore_white_space(line, main_call) >= 0) {
    return true;
  } 
  return false; 
}


struct TEST {
  struct STRING *test_function;
  struct STRING *test_name;
  struct STRING *module;
};


struct TEST *struct_TEST() {
  struct TEST *test;

  test = malloc(sizeof(struct TEST));

  return test;
}


void handle_test_case(struct CTX    *ctx, 
		      char          *line, 
		      char          *module_prefix_chars,
		      struct STRING *module) {


  s32 index = string$chars_start_with_ignore_white_space(line, module_prefix_chars); 

  if(index >= 0) {
    // printf("@@@@@@@@@@@@@@@@@@ %s @@@@@@@@@@@@@@@@@@\n", line);

    u32 end_of_function_name = string$chars_index_of_from(line, '(', index);

    // printf("found at: %d\n index: %d\n", end_of_function_name, index);

    struct TEST   *test_e         = struct_TEST(); 
    struct STRING *test_function  = struct_STRING(); 
    struct STRING *test_name      = struct_STRING(); 


    string$init(test_function);
    string$append_chars_section(test_function, 
		                index + 4, 
				end_of_function_name - index, 
				line);
    
    // NOTE: theoritically should always find $
    s32 module_end = string$chars_index_of(line, '$');

    // printf("############### module_end: %d, function_end %d\n", module_end, end_of_function_name);

    string$copy_chars(test_name, 
		      line, module_end + 1 , 
		      end_of_function_name - module_end - 1);

    u32  padding =  28 + string$len(test_name) + string$len(module);

    if(padding > ctx->test_padding) {
      ctx->test_padding = padding;
    }

    //1451Gstring$ 

    test_e->test_function = test_function;
    test_e->module        = module;
    test_e->test_name     = test_name; 
/*
    printf("test function: %s, module: %s, test_name: %s\n",
	   string$to_c_string(test_e->test_function),   
           string$to_c_string(test_e->module), 
	   string$to_c_string(test_e->test_name));
*/
    list$append(ctx->tests, test_e);

  } 
}


void get_c_module_test_prefix(struct STRING *module, 
                              struct STRING *test_prefix) {

  string$init(test_prefix);  
  
  // string$append_chars(test_prefix, "bool ");
  string$append_chars(test_prefix, "u32 ");
  string$append(test_prefix, module);
  string$append_chars(test_prefix, "_test"); 

} 


u32 append_source_file_test(struct CTX *origin_ctx,
		            struct CTX *ctx,
		            struct STRING *module, 
		            struct STRING *module_path, 
			    FILE   *super_file) { 

  struct STRING test_prefix; 
  u32  line_count   = 0; 

  get_c_module_test_prefix(module, &test_prefix);

  char *module_prefix_chars = string$to_c_string(&test_prefix); 

  FILE *module_file = file$open_r(string$to_c_string(module_path));

  char *line   = NULL;
  size_t len   = 10;
  ssize_t read;

  while ((read = getline(&line, &len, module_file)) != -1) { 
    // printf("num: %d\n", line_count);
    if(handle_main_case(line)) { 
      fprintf(super_file, "int hidden_main(int argc, char **argv) {\n");
    } else {
      //handle_test_case(ctx, line, module_prefix_chars, module);
      handle_test_case(origin_ctx, line, module_prefix_chars, module);
      fprintf(super_file, "%s",line); 
    }
    line_count ++;
  }
  file$close(module_file);

  return line_count;
}


u32 append_source_file(struct CTX *ctx,
		       struct STRING *module, 
		       struct STRING *module_path, 
		       FILE   *super_file) { 

  struct STRING test_prefix; 
  u32  line_count   = 0; 

  get_c_module_test_prefix(module, &test_prefix);

  char *module_prefix_chars = string$to_c_string(&test_prefix); 

  FILE *module_file = file$open_r(string$to_c_string(module_path));

  char *line   = NULL;
  size_t len   = 10;
  ssize_t read;

  while ((read = getline(&line, &len, module_file)) != -1) { 
    // printf("num: %d\n", line_count);
    fprintf(super_file, "%s",line); 
    line_count ++;
  }
  file$close(module_file);

  return line_count;
}


void get_build_info_path(struct CTX *ctx, struct STRING *path) {
  char *chars_base_path = "x/";
  char *chars_info_suffix = "_build.info";

  string$from_c_string(path, chars_base_path); 
  string$append(path, ctx->name); 
  string$append_chars(path, chars_info_suffix);
}


void append_tests_and_main(struct CTX *ctx, FILE *super_file) {
  // write the run test function 
  fprintf(super_file, "\n\nu32 run_tests() {\n");
  fprintf(super_file, "  u32 total_failed = 0;\n");
  fprintf(super_file, "  u32 failed_tests = 0;\n");
  
  // write the run text end 
  struct LIST *tests = ctx->tests;
  if(!list$empty(tests)) { 

    struct LIST_ITEM *list_item;
    struct STRING    *test_call;
  
    list_item = list$first(tests);  
    struct TEST *test;

    struct STRING test_label; 

    fprintf(super_file, "  printf(\"Running tests: \\n\\n\");");

    do { 
      test = list$get(list_item);

      string$init(&test_label);
      string$append_chars(&test_label, "  printf(\"[");
      string$append(&test_label, test->module); 
      string$append_chars(&test_label, "] "); 
      string$append(&test_label, test->test_name);
      //printf("############ %d ##############", ctx->test_padding);
      string$append_padding(&test_label, ' ', ctx->test_padding);
      string$append_chars(&test_label, "\");\n"); 

      fprintf(super_file, "\n%s", string$to_c_string(&test_label));


      fprintf(super_file, "  failed_tests = %s();\n", string$to_c_string(test->test_function)); 

      fprintf(super_file, "  if(failed_tests > 0) {\n");
      fprintf(super_file, "    printf(\"Failed\\n\");\n");
      fprintf(super_file, "  } else {\n");
      fprintf(super_file, "    printf(\"Success\\n\");\n");
      fprintf(super_file, "  }\n");
      fprintf(super_file, "  total_failed += failed_tests;\n"); 
		
      list_item = list$next(list_item);
    } while(list$has_next(list_item));
  } 
  fprintf(super_file, "  return failed_tests;\n");
  fprintf(super_file, "}\n");

  // write the main function
  fprintf(super_file, "\n\n");
  fprintf(super_file, "int main(int argc, char **argv) {\n");
  fprintf(super_file, "  u32 failed_tests;\n"); 
  fprintf(super_file, "  failed_tests = run_tests();\n\n");
  fprintf(super_file, "  if(failed_tests > 0) { \n"); 
  fprintf(super_file, "    printf(\"\\nTests failed: %%d\\n\\n\", failed_tests);\n");

  fprintf(super_file, "    return -1;\n");
  fprintf(super_file, "  } else {");
  fprintf(super_file, "\n    printf(\"\\nSuccess!\\n\\n\");\n"); 
  fprintf(super_file, "    return 0;\n"); 
  fprintf(super_file, "  }");
  fprintf(super_file, "\n}\n");

}

void expand_home(struct STRING *path) {
  // TODO: check if path isn't empty
  
  if(string$char_at(path, 0) == '~') {
    char *c_home = getenv("HOME");
    struct STRING home;

    string$from_c_string(&home, c_home);

     string$replace(path, 0, 1, &home); 
  } 
}

// ZZ

void git_name(struct STRING   *name,
              struct EXTERNAL *ext) {

  u32 index  = string$last_index_of_char(ext->reference, '/'); 
  u32 amount = string$len(ext->reference); 

  amount = amount - index - 5; 

  string$copy_string(name, ext->reference, index + 1, amount); 
}


void get_external_base_path(struct CTX      *origin_ctx, 
		            struct EXTERNAL *ext,
			    struct STRING   *external_base_path) {

 
  // handle git externals
  if(ext->ext_type == git || ext->ext_type == git_perm) {
    // string$from_c_string(external_base_path, "remote_src/");

    if(string$len(external_base_path) > 0) { 
      string$append_chars(external_base_path, "/");
    }
    string$append_chars(external_base_path, "remote_src/");

    struct STRING name; // TODO we could make name as part of the external struct
    string$init(&name);
    git_name(&name, ext);

    string$append(external_base_path, &name);
    return;
  }

  // handle local externals
  // TODO TODO TODO string$init(external_base_path);  TODO TODO TODO
  if(!list$empty(origin_ctx->config_local)) {
    struct LIST_ITEM *li       = list$first(origin_ctx->config_local);
    struct STRING *local_base  = list$get(li); 

    string$copy(local_base, external_base_path);

  } else {
    printf("ERROR: local base directory not set!\n");
    exit(1);
  }

  expand_home(external_base_path);
 
  string$append_chars(external_base_path, "/");
  string$append(external_base_path, ext->reference); 
} 


void get_external_ctx_path(struct EXTERNAL *ext,
		           struct STRING   *external_base_path, 
			   struct STRING   *external_ctx_path ) {

  string$init(external_ctx_path);

  string$copy(external_base_path, external_ctx_path);
  string$append_chars(external_ctx_path, "/"); 

  if(ext->ext_type == git || ext->ext_type == git_perm) {
    struct STRING name;
    string$init(&name);
    git_name(&name, ext);

    string$append(external_ctx_path, &name); 
  }
  if(ext->ext_type == local) {
    string$append(external_ctx_path, ext->reference);
  }
  string$append_chars(external_ctx_path, ".wax"); 
}


void get_super_file_path_test(struct CTX    *ctx,
		              struct STRING *super_file_path ) {

  string$init(super_file_path);

  // create the file path
  char *chars_base_path = "x/";
  char *chars_suffix = "_super_test.c";

  string$append_chars(super_file_path, chars_base_path); 
  string$append(super_file_path, ctx->name); 
  string$append_chars(super_file_path, chars_suffix); 
}


void get_super_file_path(struct CTX    *ctx,
		         struct STRING *super_file_path ) {

  string$init(super_file_path);

  // create the file path
  char *chars_base_path = "x/";
  char *chars_suffix = "_super.c";

  string$append_chars(super_file_path, chars_base_path); 
  string$append(super_file_path, ctx->name); 
  string$append_chars(super_file_path, chars_suffix); 
}


void get_file_path(struct CTX    *ctx, 
		   struct STRING *base_path, 
		   struct STRING *module,
		   struct STRING *file_path) {

   string$init(file_path);

   // TODO in future use ctx to check for file type
   char *prefix    = "src/";
   char *suffix    = ".c";
   char *seperator = "/";

   string$append(file_path, base_path);

   if(string$len(file_path) > 0) {
     string$append_chars(file_path, seperator);
   } 

   string$append_chars(file_path, prefix);
   string$append(file_path, module);
   string$append_chars(file_path, suffix);
} 


// TODO Change input for this function
void build_super_source_file_test(struct CTX    *origin_ctx, 
				  struct CTX    *sub_ctx,
				  struct STRING *base_path) { 
  struct CTX *ctx; 
  bool is_origin;

  if(sub_ctx == NULL) {
    ctx = origin_ctx;
    is_origin = true;
  } else {
    ctx = sub_ctx;
    is_origin = false;
  }

  FILE *super_file;
  FILE *info_file;

  if(is_origin == true) {
    struct STRING super_file_path;
    get_super_file_path_test(origin_ctx, &super_file_path);

    struct STRING build_info_path;
    get_build_info_path(origin_ctx, &build_info_path); 

    super_file = file$open_w(string$to_c_string(&super_file_path)); 
    info_file  = file$open_w(string$to_c_string(&build_info_path)); 

    origin_ctx->super_file = super_file;
    origin_ctx->info_file  = info_file; 

  } else { 

    super_file = origin_ctx->super_file;
    info_file  = origin_ctx->info_file; 
  } 

  struct LIST       *list_external; 
  struct EXTERNAL   *ext;

  struct STRING     external_base_path;
  struct STRING     external_ctx_path;

  struct CTX        *sub_sub_ctx;
  struct LIST_ITEM  *li;

  list_external = ctx->external;
  
  // Process the externals in this source file
  if(!list$empty(list_external)) {
  
    li = list$first(list_external);
    do {
      ext = list$get(li); 

      string$init(&external_base_path);
      string$append(&external_base_path, base_path);

      printf("~~~~~~~~~~ ext base path: %s", string$to_c_string(&external_base_path));

      get_external_base_path(origin_ctx, ext, &external_base_path);
      printf("here 1\n");
      get_external_ctx_path(ext, &external_base_path, &external_ctx_path); 
      printf("here 2\n");
     
      sub_sub_ctx = struct_CTX(); 
      printf("reading sub_ctx\n");
      read_config(sub_sub_ctx, string$to_c_string(&external_ctx_path)); 

      printf("building super sub_ctx\n");
      build_super_source_file_test(origin_ctx, sub_sub_ctx, &external_base_path);
      free(sub_sub_ctx);
      

      li = list$next(li);
    } while(list$has_next(li)); 
  } 

  struct LIST *program_files = ctx->program_files; 
  if(list$empty(program_files)) {
     return;
  } 

  // Process the files in this build 
  li = list$first(program_files);

  struct STRING file_path;
  struct STRING *module;
  u32 file_line_count = 0; 

  li = list$first(program_files);
  do { 
    module = list$get(li); 
    
    get_file_path(ctx, base_path, module, &file_path);

    file_line_count = append_source_file_test(origin_ctx, ctx, module, &file_path, super_file);
    
    fprintf(info_file, "%s\t%d\n", string$to_c_string(&file_path), file_line_count); 
    
    ctx->total_line_count += file_line_count; 

    li = list$next(li);
  } while(list$has_next(li)); 


  // append tests and main test
  if(is_origin) {
    append_tests_and_main(ctx, super_file); 

    file$close(super_file);
    file$close(info_file);
  } 
}


void create_build_info_path(struct STRING *name, struct STRING *path) {
  char *chars_base_path = "x/";
  char *chars_info_suffix = "_build.info";

  string$from_c_string(path, chars_base_path); 
  string$append(path, name); 
  string$append_chars(path, chars_info_suffix);
}


void build_super_source_file(struct CTX    *origin_ctx, 
			     struct CTX    *sub_ctx,
			     struct STRING *base_path) { 

  struct CTX *ctx; 
  bool is_origin;

  if(sub_ctx == NULL) {
    ctx = origin_ctx;
    is_origin = true;
  } else {
    ctx = sub_ctx;
    is_origin = false;
  } 

  FILE *super_file;
  FILE *info_file;

  if(is_origin == true) {
    struct STRING super_file_path;
    get_super_file_path(origin_ctx, &super_file_path);

    struct STRING build_info_path;
    get_build_info_path(origin_ctx, &build_info_path); 

    super_file = file$open_w(string$to_c_string(&super_file_path)); 
    info_file  = file$open_w(string$to_c_string(&build_info_path)); 

    origin_ctx->super_file = super_file;
    origin_ctx->info_file  = info_file; 

  } else { 

    super_file = origin_ctx->super_file;
    info_file  = origin_ctx->info_file; 
  } 

  struct LIST       *list_external; 
  struct EXTERNAL   *ext;

  struct STRING     external_base_path;
  struct STRING     external_ctx_path;

  struct CTX        *sub_sub_ctx;
  struct LIST_ITEM  *li;

  list_external = ctx->external;
  
  // Process the externals in this source file
  if(!list$empty(list_external)) {
  
    li = list$first(list_external);
    do {
      ext = list$get(li); 

      string$init(&external_base_path);
      string$append(&external_base_path, base_path); 

      get_external_base_path(origin_ctx, ext, &external_base_path);
      get_external_ctx_path(ext, &external_base_path, &external_ctx_path); 
    
      sub_sub_ctx = struct_CTX(); 

      read_config(sub_sub_ctx, string$to_c_string(&external_ctx_path)); 
      build_super_source_file(origin_ctx, sub_sub_ctx, &external_base_path);

      li = list$next(li);
    } while(list$has_next(li)); 
  } 

  struct LIST *program_files = ctx->program_files; 
  if(list$empty(program_files)) {
     return;
  } 

  // Process the files in this build 
  li = list$first(program_files);

  struct STRING file_path;
  struct STRING *module;
  u32 file_line_count = 0; 

  li = list$first(program_files);

  do { 
    module = list$get(li); 

    get_file_path(ctx, base_path, module, &file_path);

    file_line_count = append_source_file(ctx, module, &file_path, super_file);
    
    fprintf(info_file, "%s\t%d\n", string$to_c_string(&file_path), file_line_count); 
    
    ctx->total_line_count += file_line_count; 

    li = list$next(li);
  } while(list$has_next(li)); 

  // close the file
  if(is_origin) { 
    file$close(super_file);
    file$close(info_file);
  }
} 



void load_build_info_line(struct BUILD_INFO *build_info, char *line, s32 index_of_tab) {
  struct STRING *module;
  module = struct_STRING();

  u32           line_count = 0; 

  string$from_c_string_limit(module, line, index_of_tab);
  line_count = string$chars_to_u32(line, index_of_tab + 1);

  build_info->module     = module;
  build_info->line_count = line_count; 
}



void load_build_info(struct CTX *ctx) {
  // create the path
  struct STRING path;
  create_build_info_path(ctx->name, &path);
 
  // open the file
  FILE *info_file = file$open_r(string$to_c_string(&path));

  printf("opening file %s\n", string$to_c_string(&path));
 
  // make the list and add it to the context
  struct LIST *info_list = list$make();
  ctx->build_info = info_list;

  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  struct BUILD_INFO *build_info;
  s32 index_of_tab = -1;

  // read each line
  while ((read = getline(&line, &len, info_file)) != -1) {

   // split on the tab
   index_of_tab = string$chars_index_of(line, '\t');

   if(index_of_tab > 0) {
      build_info = struct_BUILD_INFO();
      load_build_info_line(build_info, line, index_of_tab);

      // add to list
      list$append(info_list, build_info);
    } 
  } 
 
  // close the file
  file$close(info_file); 
}


void print_build_info(struct CTX *ctx) {
  struct LIST *build_info; 
  build_info = ctx->build_info;

  struct BUILD_INFO *info;

  if(list$empty(build_info)) {
    printf("this is empty :-(\n");
    return;
  }

  struct LIST_ITEM *li;
  li = list$first(build_info);

  do {
   info = list$get(li);
   
   printf("file: %s line_count: %d\n", string$to_c_string(info->module), info->line_count);
   li = list$next(li);

  } while(list$has_next(li)); 
}


bool translate_line_no(struct LIST *info_list, u32 line_no, struct BUILD_INFO *result) {
  if(list$empty(info_list)) {
    return false;
  }

  u32 counted = 0;
  struct BUILD_INFO *found = NULL;
  struct LIST_ITEM *li = list$first(info_list);

  do {
    found = list$get(li);

    if(found->line_count + counted > line_no) {

      result->module     = found->module;
      result->line_count = line_no - counted;
      
      return true;
    }

    counted += found->line_count;
	     
    li = list$next(li);
  } while(list$has_next(li));

  return false;
}


void term_file() {
  printf("\033[1;315m");
}


void term_reset() {
  printf("\033[0m");
}


void handle_compiler_output(struct CTX *ctx, bool test_mode) {
  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  // QQ
  // TODO: get super file name
  struct STRING super_file_path;
  if(test_mode) {
    get_super_file_path_test(ctx, &super_file_path);
  } else {
    get_super_file_path(ctx, &super_file_path);
  }
  
  char *super_file       = string$to_c_string(&super_file_path);
  u32  super_file_len    = strlen(super_file);
  struct LIST *info_list = ctx->build_info;
  bool found             = false;

  u32  line_no;
  struct BUILD_INFO result;

  struct STRING new_line_no;

  char* blank_c = "";
  struct STRING blank;
  string$from_c_string(&blank, blank_c);

  while ((read = getline(&line, &len, stdin)) != -1) {
    if(string$chars_start_with(line, super_file)) {
       line_no = string$chars_to_u32(line, super_file_len + 1);   

       found = translate_line_no(info_list, line_no, &result);

       if(found) {
	 struct STRING line_string;
         string$from_c_string(&line_string, line);

	 // replace the file no 
	 string$u32_to_string(&new_line_no, result.line_count);
 
	 u32 replace_count = string$chars_index_of_from(line, ':', super_file_len + 1);
	 replace_count -= super_file_len; 

	 // printf("************* REPLACE COUNT %d *************\n", replace_count);

	 
	 string$replace(&line_string, super_file_len + 1, replace_count - 1, &new_line_no); 

         // replace the file name
	 //string$replace(&line_string, 0, super_file_len, result.module); 
         string$replace(&line_string, 0, super_file_len, &blank); 

	 // print
	 term_file();
	 printf("%s", string$to_c_string(result.module)); 
	 term_reset();

	 printf("%s", string$to_c_string(&line_string));
	 
       } else {
         printf("NOT%s", line); // TODO: BUG what do we do if we don't find anything?  
       } 
    } else {
      printf("%s", line);
    } 
  } 
}

/*
  if(argc == 3 && strcmp(argv[2],"-t") == 0) {
    printf("Buiding super source test\n");
    build_sub_sources_test(ctx);
    build_super_source_file_test(ctx);

    return 0;
  }

  printf("Buiding super source\n");
  build_sub_sources(ctx);
  build_super_source_file(ctx);
*/ 


u32 write_deps(struct CTX *ctx) { 
  struct LIST *external;
  external = ctx->external; 

  if(list$empty(external) == true) {
    return 0;
  } 

  // open the write to write
  FILE  *deps_file = file$open_w("x/remote.deps");

  if(deps_file == NULL) {
    printf("INTERNAL ERROR not able to create: x/remotes.deps\n");
    return 1;
  }

  struct LIST_ITEM *li = list$first(external);
  struct EXTERNAL  *ext;
  struct STRING    git_clone_cmd; 
  struct STRING    name; 
  char*            remote_dir  = "remote_src/";
  struct STRING    path;

  string$init(&git_clone_cmd);
  string$init(&name);

  do {  

    ext = list$get(li);
    if(ext->ext_type == git || ext->ext_type == git_perm) {
      // TODO handle failure of last_index_of
      /*
      u32 index  = string$last_index_of_char(ext->reference, '/'); 

      u32 amount = string$len(ext->reference); 
      amount = amount - index - 5;

      string$clear(&name); 
      string$copy_string(&name, ext->reference, index + 1, amount); 
      */
      string$clear(&name); 
      git_name(&name, ext);

      string$from_c_string(&path, remote_dir); 
      string$append(&path, &name); 
  
      bool exists   = file$exists(string$to_c_string(&path));
      bool updating = false;

      // exists but is the master branch we want to update it
      if(exists == true && string$equals_chars(ext->version, "master") == true) {
        
        string$append_chars(&git_clone_cmd, "git -C remote_src/"); 
        string$append(&git_clone_cmd, &name); 
        string$append_chars(&git_clone_cmd, " pull"); 

        // write the reference to the file
        printf("%s\n", string$to_c_string(&git_clone_cmd)); 
        fprintf(deps_file, "%s\n", string$to_c_string(&git_clone_cmd));

	updating = true;
      }
      // see if the branch has already been retrieved
      if(exists == false) {
        // append the git clone prefix
        string$append_chars(&git_clone_cmd, "git -C remote_src"); 

        // append the tag/branch
        string$append_chars(&git_clone_cmd, " clone --branch "); 
        string$append(&git_clone_cmd, ext->version); 
        string$append_chars(&git_clone_cmd, " --single-branch --depth 1 "); 

        // append the repo 
        string$append(&git_clone_cmd, ext->reference); 

        // write the reference to the file
        printf("%s\n", string$to_c_string(&git_clone_cmd));
        fprintf(deps_file, "%s\n", string$to_c_string(&git_clone_cmd));

	updating = true;
      }

      if(updating == true) {
        fprintf(deps_file, "git_add remote_src/%s\n", string$to_c_string(&name));

      } 

      // run deps on the deps to make sure sub dependencies are updated
      fprintf(deps_file, "(cd remote_src/%s && deps)\n", string$to_c_string(&name));

      // reset the string for the next line
      string$clear(&git_clone_cmd); 
    }

    li = list$next(li);

  } while(list$has_next(li));

  file$close(deps_file);

  return 0;
}


// ALL THE TESTS ---------------


u32 wax_test$get_c_file_path() { 
  char *c_file_path = "hello";
  struct STRING file_path;
  string$from_c_string(&file_path, c_file_path);

  char *c_result = "src/hello.c";
  struct STRING result;
  string$from_c_string(&result, c_result);

  get_c_file_path(&file_path);

  if(!string$equals(&file_path, &result)) {
    printf("***FAILED*** result no expected, got: ");   
    printf("[");   
    print(&file_path);
    printf("]");   
    printf(" instead of [src/hello/hello.c]\n");
    return 1; 
  }

  return 0;
} 


void setup() {
  string$from_c_string(&LABEL_NAME,     C_LABEL_NAME);
  string$from_c_string(&LABEL_TEST,     C_LABEL_TEST);
  string$from_c_string(&LABEL_PROGRAM,  C_LABEL_PROGRAM);
  string$from_c_string(&LABEL_LOCAL,    C_LABEL_LOCAL);
  string$from_c_string(&LABEL_EXTERNAL, C_LABEL_EXTERNAL);
}


int main(int argc, char **argv) { 
  if(argc < 2) {
    printf("expected: wax file.\n");
    return -1;
  }

  setup(); 

  struct CTX *ctx; 
  ctx = struct_CTX();

  // read global config
  read_global_config(ctx); 
  read_config(ctx, argv[1]);

  // TODO validate config file
  //
  // print_CTX(ctx); 
  if(argc == 3 && strcmp(argv[2],"-deps") == 0) {
    write_deps(ctx);
    return 0;
  }

  if(argc == 3 && strcmp(argv[2],"-ot") == 0) {
    printf("Handling compiler output!\n");
    load_build_info(ctx);
    print_build_info(ctx);

    if(ftell(stdin) < 0) {
      handle_compiler_output(ctx, true);
      return -1;
    }

    return 0;
  }

  if(argc == 3 && strcmp(argv[2],"-o") == 0) {
    printf("Handling compiler output!\n");
    load_build_info(ctx);
    print_build_info(ctx);

    if(ftell(stdin) < 0) {
      handle_compiler_output(ctx, false);
      return -1;
    }

    return 0;
  }

  struct STRING base_path; 
  string$init(&base_path);
  if(argc == 3 && strcmp(argv[2],"-t") == 0) { 

    build_super_source_file_test(ctx, NULL, &base_path); 
    return 0;
  }

  printf("Buiding super source\n");
  build_super_source_file(ctx, NULL, &base_path);

  return 0;
} 
