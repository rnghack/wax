### Overview
wax build is an experimental (not production ready) build system that is under development, currently supporting the c programming language. Supported operating systems: linux, freebsd and macos.

Wax has the following capabilities:
1) generate a test build to run unit tests
2) local and remote git dependencies
3) no need to use the languages built in library or module system to separate your code
4) all code is accessible from all parts of your program, no need for includes or imports

### How it works
Wax preprocesses and aggregates your source files into a super source file and parses that to the compiler.

It then post processes the compiler output to translate any line number errors.


### Installation
You will need clang (freebsd, macos) or gnu c compiler (linux) for the installion to work.

Prior to installation create and set you $LOCAL_BIN directory, and add $LOCAL_BIN to your shell path.

wax installer will compile the wax program *install_src/wax_super.c* and then will copy the scripts and the wax program to $LOCAL_BIN
```
git clone https://gitlab.com/rnghack/wax.git
cd wax
./install
```


### Commands
**b** - build source files into an executable

**i** - build source files into an executable and copy it to $LOCAL_BIN

**r** - build source files into an executable and run it

**t** - build sources into a executable that runs the tests instead of the main program

**deps** - downloads or updates remote git repos into the remote_src directory


### .wax config file

The wax config file is located in the root folder of your build, it specifies the name and type and files in your build along with the external dependancies of your program.

It is important to note all files that you want to be included in your build should be specified under the *program:* header, wax will concatinate the files in the order you specify in the program list which makes it crucial that you consider your depedancies in regards to the order.

```
name:
wax

type:
c

external:
#local          std_c
#git_perm   https://gitlab.com/rnghack/std_c.git master
git   https://gitlab.com/rnghack/std_c.git master

program:
wax
```
_#_        a commented line

_local_              a wax build in your local folder which is specified in _~/.wax.cfg_ see below

_git_                a git repository that is retrieved or updated when the deps command is run

*git_perm*           same as git with the addition that the files retrieved are added to the current repo with git add




### Tests
For wax to find your test functions they need to be prefixed with <file_name>_test$<function_name>


### Configuration file
~/.wax.cfg

is an optional file where you can set your local directory for local dependancies 
```
local:
<directory>
```

### Notable missing features

1) Does not de duplicate same includes in external includes
2) Does not handle different versions of the same external used in mutiple places